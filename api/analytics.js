import fetch from "isomorphic-unfetch";
import handler from "./handler";
const BASE_URL = process.env.API_URL;

const analytics = {
  getBlog: async function(blogId, period) {
    let options = {
      method: "GET",
      mode: "cors",
      credentials: "include"
    };
    let query = period ? `?period=${period}` : "";
    let response;
    try {
      response = await fetch(`${BASE_URL}/analytics/blog${query}`, options);
    } catch (error) {
      return handler.exception(error);
    }
    return await handler.response(response);
  }
};

export default analytics;
