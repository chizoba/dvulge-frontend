import fetch from "isomorphic-unfetch";
import handler from "./handler";
const BASE_URL = process.env.API_URL;

const blog = {
  get: async function(slug) {
    let response;
    try {
      response = await fetch(`${BASE_URL}/blogs/${slug}`, {
        mode: "cors",
        credentials: "include"
      });
    } catch (error) {
      return handler.exception(error);
    }
    return await handler.response(response);
  },

  create: async function(data) {
    let response;
    try {
      response = await fetch(`${BASE_URL}/blogs`, {
        method: "POST",
        mode: "cors",

        body: data,
        credentials: "include"
      });
    } catch (error) {
      return handler.exception(error);
    }
    return await handler.response(response);
  },

  update: async function(data) {
    let response;
    try {
      response = await fetch(`${BASE_URL}/blogs`, {
        method: "PUT",
        mode: "cors",
        credentials: "include",
        body: data
      });
    } catch (error) {
      return handler.exception(error);
    }
    return await handler.response(response);
  },

  delete: async function() {
    let response;
    try {
      response = await fetch(`${BASE_URL}/blogs`, {
        method: "DELETE",
        mode: "cors",
        credentials: "include"
      });
    } catch (error) {
      return handler.exception(error);
    }
    return await handler.response(response);
  },
  checkBlogSlugIsUnique: async function(slug) {
    let response;
    try {
      response = await fetch(
        `${BASE_URL}/blogs/check/slug-exist?slug=${slug}`,
        {
          mode: "cors",
          credentials: "include"
        }
      );
    } catch (error) {
      return handler.exception(error);
    }
    return await handler.response(response);
  }
};

export default blog;
