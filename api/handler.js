const response = async (response)=>{ //returned
  let status = response.status;
  let result = await response.json()
  return {data: result.data, error: result.error, status}
}

const exception = (error)=>{//returned when request could not be completed by fetch API
  //log excpetion
  return {data:null, error: "Request failed", status:500}
}

export default {
  exception,
  response
}
