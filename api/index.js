import session from "./session";
import user from "./user";
import blog from "./blog";
import post from "./post";
import analytics from "./analytics";
import like from "./like";
import upload from "./upload";

export default { session, user, blog, post, analytics, like, upload };
