import fetch from "isomorphic-unfetch";
import handler from "./handler";
const BASE_URL = process.env.API_URL;

const like = {
  get: async function(postId) {
    let options = {
      method: "GET",
      mode: "cors",
      credentials: "include"
    };
    let response;
    try {
      response = await fetch(`${BASE_URL}/like/${postId}`, options);
    } catch (error) {
      return handler.exception(error);
    }
    return await handler.response(response);
  },
  create: async function(postId) {
    let response;
    try {
      response = await fetch(`${BASE_URL}/like/${postId}`, {
        method: "POST",
        mode: "cors",
        credentials: "include",
        headers: {
          "Content-Type": "application/json"
        }
      });
    } catch (error) {
      return handler.exception(error);
    }
    return await handler.response(response);
  },
  delete: async function(postId) {
    let response;
    try {
      response = await fetch(`${BASE_URL}/like/${postId}`, {
        method: "DELETE",
        mode: "cors",
        credentials: "include"
      });
    } catch (error) {
      return handler.exception(error);
    }
    return await handler.response(response);
  }
};
export default like;
