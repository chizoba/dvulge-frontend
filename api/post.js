import fetch from "isomorphic-unfetch";
import handler from "./handler";
const BASE_URL = process.env.API_URL;
let post = {
  get: async function(slug) {
    //add options
    let response;
    try {
      response = await fetch(`${BASE_URL}/posts/${slug}`, {
        mode: "cors",
        credentials: "include"
      });
    } catch (error) {
      return handler.exception(error);
    }
    return await handler.response(response);
  },

  create: async function(data, cookie) {
    let options = {
      method: "POST",
      mode: "cors",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json"
      },
      credentials: "include"
    };
    if (cookie) {
      options.headers = { cookie };
    }
    let response;
    try {
      response = await fetch(`${BASE_URL}/posts`, options);
    } catch (error) {
      return handler.exception(error);
    }
    return await handler.response(response);
  },
  update: async function(slug, data) {
    let options = {
      method: "PUT",
      mode: "cors",
      body: data,
      credentials: "include"
    };
    let response;
    try {
      response = await fetch(`${BASE_URL}/posts/${slug}`, options);
    } catch (error) {
      return handler.exception(error);
    }
    return await handler.response(response);
  },
  list: async function(query) {
    //status no longer used
    const ALLOWED_QUERY_KEYS = [
      "editor",
      "blog",
      "page",
      "search",
      "tag",
      "active",
      "category",
      "status",
      "published"
    ];
    let queryString = Object.keys(query)
      .map(key => {
        if (ALLOWED_QUERY_KEYS.includes(key) && query[key] !== "") {
          let value = query[key];
          if (key === "category") key = "filter"; //map blog category to post's tags
          return key + "=" + value;
        } else {
          return;
        }
      })
      .join("&");
    let response;
    try {
      response = await fetch(`${BASE_URL}/posts?${queryString}`);
    } catch (error) {
      return handler.exception(error);
    }
    return await handler.response(response);
  },

  publish: async function(slug) {
    let response;
    try {
      response = await fetch(`${BASE_URL}/posts/${slug}/publish`, {
        method: "PUT",
        mode: "cors",
        credentials: "include"
      });
    } catch (error) {
      return handler.exception(error);
    }
    return await handler.response(response);
  },
  delete: async function(slug) {
    let response;
    try {
      response = await fetch(`${BASE_URL}/posts/${slug}`, {
        method: "DELETE",
        mode: "cors",
        credentials: "include"
      });
    } catch (error) {
      return handler.exception(error);
    }
    return await handler.response(response);
  },
  recommend: async function(slug) {
    let response;
    try {
      response = await fetch(`${BASE_URL}/posts/${slug}/recommendations`, {
        mode: "cors",
        credentials: "include"
      });
    } catch (error) {
      return handler.exception(error);
    }
    return await handler.response(response);
  }
};
export default post;
