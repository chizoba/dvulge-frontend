import fetch from "isomorphic-unfetch";
import handler from "./handler";
const BASE_URL = process.env.API_URL;

const session = {
  get: async function(cookie) {
    let options = {
      method: "GET",
      mode: "cors",
      credentials: "include"
    };
    if (cookie) options.headers = { cookie }; //add header.cookie to options
    let response;
    try {
      response = await fetch(`${BASE_URL}/session`, options);
    } catch (error) {
      return handler.exception(error);
    }
    return await handler.response(response);
  },
  create: async function(token) {
    let response;
    try {
      response = await fetch(`${BASE_URL}/session`, {
        method: "POST",
        mode: "cors",
        credentials: "include",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(token)
      });
    } catch (error) {
      return handler.exception(error);
    }
    return await handler.response(response);
  },
  delete: async function() {
    let response;
    try {
      response = await fetch(`${BASE_URL}/session`, {
        method: "DELETE",
        mode: "cors",
        credentials: "include"
      });
    } catch (error) {
      return handler.exception(error);
    }
    return await handler.response(response);
  }
};
export default session;
