import fetch from "isomorphic-unfetch";
import handler from "./handler";
const BASE_URL = process.env.API_URL;

async function upload(data) {
  let response;
  try {
    response = await fetch(`${BASE_URL}/resource`, {
      method: "POST",
      mode: "cors",
      credentials: "include",
      body: data
    });
  } catch (error) {
    return handler.exception(error);
  }
  return await handler.response(response);
}

export default upload;
