import fetch from "isomorphic-unfetch";
import handler from "./handler";
const BASE_URL = process.env.API_URL;
console.log(BASE_URL);
const user = {
  get: async function(id) {
    let response;
    try {
      response = await fetch(`${BASE_URL}/users/${id}`, {
        mode: "cors",
        credentials: "include"
      });
    } catch (error) {
      return handler.exception(error);
    }
    return await handler.response(response);
  },

  create: async function(data) {
    let response;
    try {
      response = await fetch(`${BASE_URL}/users`, {
        method: "POST",
        mode: "cors",
        credentials: "include",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
      });
    } catch (error) {
      return handler.exception(error);
    }
    return await handler.response(response);
  },

  update: async function(data) {
    let response;
    try {
      response = await fetch(`${BASE_URL}/users`, {
        method: "PUT",
        credentials: "include",
        body: data,
        mode: "cors"
      });
    } catch (error) {
      return handler.exception(error);
    }
    return await handler.response(response);
  },

  createAuthLink: async function(data) {
    let response;
    try {
      response = await fetch(`${BASE_URL}/users/create-auth-link`, {
        method: "POST",
        mode: "cors",
        headers: {
          "Content-Type": "application/json"
        },
        credentials: "include",
        body: JSON.stringify(data)
      });
    } catch (error) {
      return handler.exception(error);
    }
    return await handler.response(response);
  },

  delete: async function() {
    let response;
    try {
      response = await fetch(`${BASE_URL}/users`, {
        method: "DELETE",
        mode: "cors",
        credentials: "include"
      });
    } catch (error) {
      return handler.exception(error);
    }
    return await handler.response(response);
  }
};

export default user;
