import React from "react";
import { Router } from "../routes";
import api from "../api";
import { Skeleton, Spin } from "antd";
import PageLoader from "./PageLoader";
import { AUTH_LOGIN_SUCCESS } from "../store/constants";

export default function withAuth(
  Component,
  authRequired = false,
  blogRequired = false
) {
  return class Authenticated extends React.Component {
    static async getInitialProps(ctx) {
      // console.log("get init---");
      const { store, req, res } = ctx;
      //get current pathname
      // const pathname = req ? req.path : useRouter.asPath;
      // Check if Page has a `getInitialProps`; if so, call it.
      let { isAuthenticated, user } = store.getState().auth.payload;

      if (!isAuthenticated) {
        let cookie = "";
        if (req) cookie = req.headers.cookie;
        const response = await api.session.get(cookie);
        if (response.data !== null) {
          store.dispatch({
            type: AUTH_LOGIN_SUCCESS,
            payload: response.data.user
          });
          isAuthenticated = true;
          user = response.data.user;
        }
      }

      if (authRequired && !isAuthenticated) {
        //if page requires authenticate. Redirect the user if not authenticated
        if (res) return res.redirect("/");
        return Router.replaceRoute("index");
      }
      if (authRequired && blogRequired && (!user.blog || !user.blog.slug)) {
        //if page requires blog. Redirect the user to create blog
        if (res) return res.redirect("/i/create-blog");
        return Router.replaceRoute("createBlog");
      }
      // console.log(pathname);
      if (req && isAuthenticated && req.path === "/") {
        return res.redirect(`/${user.blog.slug}`);
      }

      let pageProps = {};

      if (Component.getInitialProps) {
        pageProps = await Component.getInitialProps(ctx);
      }
      // Return props.
      return { ...pageProps };
    }

    constructor(props) {
      super(props);

      this.state = {
        isLoading: true
      };
    }

    componentDidMount() {
      this.setState({ isLoading: false });
    }

    render() {
      return (
        <div>
          {this.state.isLoading ? (
            <PageLoader />
          ) : (
            <Component {...this.props} />
          )}
        </div>
      );
    }
  };
}
