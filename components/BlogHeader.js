import { Avatar, Icon, Button, Menu, Dropdown, Popover } from "antd";

export default function BlogHeader({ logo, title, description, editor }) {
  editor = editor ? editor : {};
  const { twitter, facebook, instagram } = editor;
  const editorAvatarPlaceholder = editor.fullName
    ? editor.fullName.charAt(0).toUpperCase()
    : "";

  const editorProfile = (
    <div style={{ maxWidth: 200 }}>
      <Avatar
        size={72}
        style={{
          display: "block",
          margin: "auto",
          backgroundColor: editor.avatar ? "transparent" : "#f9f9f9"
        }}
        src={editor.avatar}
      >
        {editorAvatarPlaceholder}
      </Avatar>

      <br />
      <p>{editor.bio}</p>
    </div>
  );

  const socialProfiles = (
    <Menu>
      {twitter && (
        <Menu.Item>
          <a target="_blank" rel="noopener noreferrer" href={twitter}>
            <Icon type="twitter" /> Twitter
          </a>
        </Menu.Item>
      )}
      {facebook && (
        <Menu.Item>
          <a target="_blank" rel="noopener noreferrer" href={facebook}>
            <Icon type="facebook" /> Facebook
          </a>
        </Menu.Item>
      )}
      {instagram && (
        <Menu.Item>
          <a target="_blank" rel="noopener noreferrer" href={instagram}>
            <Icon type="instagram" /> Instagram
          </a>
        </Menu.Item>
      )}
      {!instagram && !twitter && !facebook && <Menu.Item>-</Menu.Item>}
    </Menu>
  );

  return (
    <div>
      <div className="blogHeaderContainer">
        <Avatar
          size={150}
          src={logo}
          style={{
            backgroundColor: logo ? "transparent" : "#f9f9f9",
            marginBottom: 10
          }}
        />
        <p>
          <strong>{title}</strong>
        </p>

        <div className="description">
          <p>{description}</p>

          <div>
            <Popover
              placement="bottomLeft"
              trigger="click"
              content={editorProfile}
              title="Creator"
            >
              <span>by </span>
              <span className="link" style={{ cursor: "pointer" }}>
                {editor.fullName}.
              </span>
            </Popover>
            <Dropdown
              overlay={socialProfiles}
              placement="bottomLeft"
              trigger={["click", "hover"]}
            >
              <Button
                ghost
                type="primary"
                size="small"
                style={{ marginLeft: 5 }}
              >
                Follow <Icon type="down" />
              </Button>
            </Dropdown>
          </div>
        </div>
      </div>
      <style jsx>{`
        .blogHeaderContainer {
          min-height: 150px;
          margin: 20px auto;
          text-align: center;
          padding: 10px;
        }

        .description {
          font-size: 14px;
          max-width: 400px;
          margin: 10px auto;
        }
      `}</style>
    </div>
  );
}

BlogHeader.defaultProps = {
  description: "",
  title: "",
  logo: "",
  editor: { fullName: "" }
};
