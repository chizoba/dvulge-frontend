import moment from "moment";
import { Link } from "../routes";
import { Icon } from "antd";
function FlatPostCard({ post, onPublish, onDelete }) {
  const { title, slug, blog, dateCreated, datePublished, isPublished } = post;

  let subtitle = isPublished
    ? `Created ${moment(dateCreated)
        .format("ll")
        .toString()} / Published  ${moment(datePublished)
        .format("ll")
        .toString()} `
    : `created ${moment(dateCreated)
        .format("ll")
        .toString()}.`;

  return (
    <div>
      <div className={"postCardContainer"}>
        <div>
          <Link
            route="post"
            params={{
              postSlug: slug,
              blogSlug: blog.slug
            }}
          >
            <a className="title">
              <strong>{title}</strong>
            </a>
          </Link>
          <p className={"subtitle"}>{subtitle}</p>
        </div>
        <div className={"cardFooter"}>
          <Link
            route="compose"
            params={{
              slug
            }}
          >
            <a>
              <Icon type="edit" /> Edit
            </a>
          </Link>
          {/*}{!isPublished && (
            <a
              onClick={() => {
                onPublish(post);
              }}
            >
              <Icon type="to-top" /> Publish
            </a>
          )}*/}
          <a
            onClick={() => {
              onDelete(slug);
            }}
          >
            <Icon type="delete" /> Delete
          </a>
        </div>
      </div>
      <style>
        {`
              .postCardContainer{
                  // margin: 20px 0;
                  padding: 10px 5px;
                  border-bottom: 1px solid #ccc;
                  max-width: 700px;
              }
              .postCardContainer a {
                color: #555;
              }
              .postCardContainer:hover{
                  background: #fafafa;

              }
              .folderHeader{
                font-size:14px;
              }
              .title:hover {
                color:dodgerblue;
              }
              .title:active {
                color:inherit;
              }
              .subtitle {
                  font-size: 12px;
                  color: #777;
              }
              .cardFooter a {
                  margin: 10px ;
                  font-size: 14px;
              }
              .cardFooter a:hover {
                color:dodgerblue;
              }
              .cardFooter a:active {
                color:inherit;
              }


              `}
      </style>
    </div>
  );
}

export default FlatPostCard;
