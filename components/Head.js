import NextHead from "next/head";
import { getAppURL } from "../utils";
import { Fragment } from "react";
function Head({
  title,
  description,
  image,
  type,
  url,
  allowIndex,
  displayGoogleAd
}) {
  image = image ? image : "https://cdn.dvulge.com/logo/logo_800x800.png";
  title = title ? title : "Dvulge";
  type = type ? type : "website";
  url = url ? url : getAppURL();
  description = description ? description : "";

  return (
    <NextHead>
      <meta charSet="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <title>{title}</title>
      <meta name="description">{description}</meta>
      <link rel="canonical" href={url} />
      <meta property="og:title" content={title} />
      <meta property="og:description" content={description} />
      <meta property="og:image" content={image} />
      <meta property="og:site_name" content={title} />
      <meta property="og:type" content={type} />
      <meta property="og:url" content={url} />
      <meta name="twitter:card" content="summary_large_image" />
      {/*favicons*/}

      <link
        rel="apple-touch-icon"
        sizes="180x180"
        href="/static/favicons/apple-touch-icon.png"
      />
      <link
        rel="icon"
        type="image/png"
        sizes="32x32"
        href="/static/favicons/favicon-32x32.png"
      />
      <link
        rel="icon"
        type="image/png"
        sizes="16x16"
        href="/static/favicons/favicon-16x16.png"
      />
      <link rel="manifest" href="/static/favicons/site.webmanifest" />
      {!allowIndex && <meta name="robots" content="noindex, nofollow" />}
    </NextHead>
  );
}

Head.defaultProps = {
  title: "",
  description: "",
  image: "",
  url: "",
  type: "",
  allowIndex: true
};
export default Head;
