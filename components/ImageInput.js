import React, {
  useCallback,
  useEffect,
  useState,
  useMemo,
  useRef
} from "react";
import Dropzone from "react-dropzone";
import { Button, Icon } from "antd";
// import Croppie from 'react-croppie'
import Cropper from "react-cropper";
// import Cropper from 'react-cropper';
import "cropperjs/dist/cropper.css";
import { validateImage } from "../utils";

//props width, height,
export default function ImageInput(props) {
  const width = props.width || 200;
  const height = props.height || 200;
  // let [image, setImage] = useState('') //should be gotten from prop

  let [preview, setPreview] = useState("");
  let image = useMemo(() => props.image, [props.image]);
  let [inputKey, setInputKey] = useState(Date.now());
  const cropper = useRef("cropper");
  const fileUploader = useRef("fileUploader");
  const maxSize = 5242880;

  const _crop = () => {
    // console.log(cropper.current.getCroppedCanvas().toDataURL());
  };

  const _finishCrop = () => {
    setPreview("");
    //get cropped image and convert to blob
    // cropper.current.getCroppedCanvas().toBlob((blob)=>{
    //    props.onDone(blob)
    // })
    let img = cropper.current.getCroppedCanvas().toDataURL();
    props.onDone(img);
    //clear input field
    setInputKey(Date.now());
  };
  const _cancelCrop = () => {
    setPreview("");
    //clear input field
    setInputKey(Date.now());
  };
  const _deleteImage = () => {
    //use prop
    props.onDelete();
  };

  const _handleFileUpload = event => {
    let image = event.target.files[0];
    if (!image) {
      return;
    }
    try {
      validateImage(image);
    } catch (error) {
      setInputKey(Date.now());
      if (props.onError) props.onError(error);
      return;
    }
    setPreview(URL.createObjectURL(event.target.files[0]));
  };
  // style={{ minWidth: width, minHeight: height }}
  return (
    <div>
      <div className="inputContainer" style={{ width: width, minHeight: 40 }}>
        <div className="imageContainer">
          <input
            key={inputKey}
            ref={fileUploader}
            id="input"
            type="file"
            accept=".jpg, .png, .gif, .jpeg"
            onChange={_handleFileUpload}
            style={{ display: "none" }}
          />

          {preview && (
            <div className="previewContainer">
              <Cropper
                ref={cropper}
                src={preview}
                viewMode={3}
                style={{ height, width }}
                // Cropper.js options

                cropBoxResizable={false}
                minCropBoxWidth={width}
                minCropBoxHeight={height}
                guides={false}
                crop={_crop}
              />
              <br />
              <div style={{ marginBottom: 10 }}>
                <Button ghost onClick={_finishCrop} type="primary">
                  Done
                </Button>
                <Button ghost onClick={_cancelCrop} type="danger">
                  Cancel
                </Button>
              </div>
            </div>
          )}
          {!preview && image && (
            <div className="imgContainer">
              <img src={image} style={{ width, height }} />
            </div>
          )}
        </div>
        {!preview && (
          <div className="imageInputButtons">
            <div className="uploadButton">
              <Button
                shape="circle"
                icon="upload"
                style={{ margin: "auto" }}
                onClick={() => fileUploader.current.click()}
              />
              <p>upload</p>
            </div>
            {image && (
              <div className="deleteButton">
                <Button
                  shape="circle"
                  style={{ margin: "auto" }}
                  icon="delete"
                  onClick={_deleteImage}
                />
                <p>Delete</p>
              </div>
            )}
          </div>
        )}
      </div>
      <style jsx>
        {`
          .imageContainer {
            // background: #f3f3f3;
            border: 1px solid #f3f3f3;
            position: relative;
            border-radius: 5px;
            display: inline-block;
          }
          .imageInputButtons {
            // position: absolute;
            // bottom: 0px;
            width: 100%;
            text-align: center;
            // margin: 10px;
          }
          .uploadButton,
          .deleteButton {
            display: inline-block;
            margin: 10px;
            font-size: 14px;
            text-align: center;
          }

          input {
            display: none !important;
          }
          .previewContainer {
            text-align: center;
          }
          img {
            border-radius: 5px;
          }
        `}
      </style>
    </div>
  );
}
