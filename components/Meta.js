import Head from "next/head";
const Meta = props => (
  <Head>
    <title>{props.title}</title>
    <meta name="description" content={props.desc} />
    <meta property="og:type" content="website" />
    <meta name="og:title" property="og:title" content={props.title} />
    <meta
      name="og:description"
      property="og:description"
      content={props.desc}
    />
    <meta property="og:site_name" content="Dvulge" />
    <meta property="og:url" content={`${props.url}`} />
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content={props.title} />
    <meta name="twitter:description" content={props.desc} />

    {props.css && <link rel="stylesheet" href={`${props.css}`} />}
    {props.image ? (
      <meta property="og:image" content={`${props.image}`} />
    ) : (
      <meta
        property="og:image"
        content="https://cdn.dvulge.com/logo/logo_800x800.png"
      />
    )}
  </Head>
);
export default Meta;
