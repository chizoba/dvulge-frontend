import { Fragment, useState } from "react";
export default function PostImage({ src, className, style, caption, width }) {
  return (
    <Fragment>
      <img src={src} className={className} width={width} title={caption} />
      <figcaption className="postImageCaption">{caption}</figcaption>
    </Fragment>
  );
}
