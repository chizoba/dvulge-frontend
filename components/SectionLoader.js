import { Icon } from "antd";
export default function sectionLoader({ height, width }) {
  height = height ? height : "100px";
  width = width ? width : "100%";
  return (
    <div
      style={{
        height,
        width,
        display: "flex",
        justifyContent: "center",
        alignItems: "center"
      }}
    >
      <Icon type="loading" size={64} />
    </div>
  );
}
