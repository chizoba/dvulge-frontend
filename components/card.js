import Link from "next/link";
import moment from "moment";

export default function Card({
  title,
  image,
  content,
  blogSlug,
  datePublished,
  postSlug
}) {
  const subtitle = `${moment(datePublished)
    .format("ll")
    .toString()} `;
  return (
    <div>
      <div className="cardContainer">
        <Link
          href={{ pathname: "/post", query: { postSlug, blogSlug } }}
          as={`/${blogSlug}/post/${postSlug}`}
        >
          <a>
            <div className="cardThumbnailWrapper">
              <img className="cardThumbnail" src={image} />
            </div>

            <div className="cardBody">
              <p className="cardTitle">
                {title.length > 60 ? `${title.slice(0, 60)}...` : title}
              </p>

              <p className="cardSubtitle">{subtitle} </p>
              <div>
                {content.length > 120 ? `${content.slice(0, 120)}...` : content}
              </div>
            </div>
          </a>
        </Link>
      </div>
      <style jsx>{`
        .cardContainer {
          max-height: 500px;
          width: 300px;
          margin: 10px auto;
          padding: 10px;
          overflow: hidden;

          font-size: 14px;
          background-color: white;
        }
        .cardTitle {
          font-size: 20px;
          color: #555;
        }
        .cardSubtitle {
          font-size: 12px;
          color: #777;
        }
        .cardThumbnailWrapper {
          text-align: center;
          background: #fafafa;
          height: 175px;
          width: 260px;
          margin: 10px auto;
          border: 1px solid #eee;
          border-radius: 3px;
          margin-bottom: 15px;
        }
        .cardThumbnail {
          width: 100%;
          height: 100%;
          border-radius: 3px;
        }
      `}</style>
    </div>
  );
}

Card.defaultProps = {
  title: "",
  content: "",
  image: "",
  blogSlug: "",
  postSlug: "",
  datePublished: Date.now()
};
