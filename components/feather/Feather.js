//Text editor
import { Component } from "react";
import {
  EditorState,
  Entity,
  ContentState,
  CompositeDecorator,
  RichUtils,
  Modifier,
  AtomicBlockUtils,
  convertToRaw,
  convertFromRaw
} from "draft-js";
import "draft-js/dist/Draft.css";
import getBlockStyle from "./utils/blockStyles";
import {
  _onUndo,
  _onRedo,
  _onBoldClick,
  _onItalicClick,
  _onHighlightClick,
  _onBlockquote,
  _onAddLink,
  _onAddSmiley,
  onAddImage,
  _onChangeBlockType
} from "./utils/handleCommands";
import { ReleaseIcon } from "../index";
import onPastedFiles from "./utils/onPastedFiles";
import onDroppedFiles from "./utils/onDroppedFiles";
import onPastedText from "./utils/onPastedText";
import blockRenderMap from "./utils/blockRenderMap";
import importerConfig from "./utils/htmlImporterConfig";
import exporterConfig from "./utils/htmlExporterConfig";
import customKeyBindings from "./utils/customKeyBindings";
import { convertFromHTML, convertToHTML } from "draft-convert";
import { stateFromHTML } from "draft-js-import-html";
import Editor, { composeDecorators } from "draft-js-plugins-editor"; //use this editor to add plugins
import {
  Menu,
  Input,
  message,
  Dropdown,
  Icon,
  Button,
  Popover,
  Upload
} from "antd";
const { TextArea } = Input;
import createHighlightPlugin from "./plugins/highlightPlugin";
import linkPlugin, { Link } from "./plugins/linkPlugin";
import createImagePlugin from "./plugins/imagePlugin";
import createLinkifyPlugin from "draft-js-linkify-plugin";
import "draft-js-linkify-plugin/lib/plugin.css";
import createAutoListPlugin from "draft-js-autolist-plugin";
const autoListPlugin = createAutoListPlugin();
import createLinkPlugin from "draft-js-anchor-plugin";
import "emoji-mart/css/emoji-mart.css";
import { Picker } from "emoji-mart";
import Sticky from "react-stickynode";
import Quote from "react-ionicons/lib/MdQuote";
// import createImagePlugin2 from "draft-js-image-plugin";
import createBlockDndPlugin from "draft-js-drag-n-drop-plugin";
import "draft-js-focus-plugin/lib/plugin.css";
import createResizeablePlugin from "draft-js-resizeable-plugin";

import createFocusPlugin from "draft-js-focus-plugin";
const focusPlugin = createFocusPlugin();
const resizeablePlugin = createResizeablePlugin();
const blockDndPlugin = createBlockDndPlugin();

const decorator = composeDecorators(
  resizeablePlugin.decorator,
  focusPlugin.decorator,
  blockDndPlugin.decorator
);
const linkifyPlugin = createLinkifyPlugin({
  target: "_blank" // default is '_self'
});
// const imagePlugin = createImagePlugin2({
//   decorator
// });
const localImagePlugin = createImagePlugin({ decorator });

const highlightPlugin = createHighlightPlugin();

export default class MyEditor extends React.Component {
  constructor(props) {
    super(props);
    this.editorRef = React.createRef();
    this.state = {
      editorState: EditorState.createEmpty(),
      title: "",
      imageURL: "",
      showImagePopover: false,
      showLinkPopover: false,
      imageCaption: "",
      linkURL: "",
      imageInputKey: Date.now(),
      hasBold: false,
      hasBold: false,
      hasItalic: false,
      hasUnderline: false,
      hasStrikethrough: false,
      placeholderClassName: "",
      hasHighlight: false,
      isOrderedList: false,
      isUnorderedList: false,
      hasBlockquote: false,
      hasSubHeading1: false,
      shouldNavStick: false
    };
    this.plugins = [
      highlightPlugin,
      linkPlugin,
      linkifyPlugin,
      // imagePlugin,

      focusPlugin,
      resizeablePlugin,

      blockDndPlugin,
      localImagePlugin,
      autoListPlugin
    ];
    this._onChange = this._onChange.bind(this);
    this.focus = this.focus.bind(this);
    this.handleKeyCommand = this.handleKeyCommand.bind(this);
    this._handleCommand = this._handleCommand.bind(this);
    this._handleAddLink = this._handleAddLink.bind(this);
  }

  //handle editor state changes
  _onChange = editorState => {
    const selection = editorState.getSelection();
    const currentFocus = selection.getFocusKey();
    const inlineStyle = editorState.getCurrentInlineStyle(currentFocus);
    const hasBold = inlineStyle.has("BOLD");
    const hasItalic = inlineStyle.has("ITALIC");
    const hasUnderline = inlineStyle.has("UNDERLINE");
    const hasStrikethrough = inlineStyle.has("STRIKETHROUGH");
    const hasHighlight = inlineStyle.has("HIGHLIGHT");
    //get blovk
    const contentState = editorState.getCurrentContent();
    const blockType = contentState.getBlockForKey(currentFocus).getType();
    const hasBlockquote = blockType === "blockquote";
    const hasSubHeading1 = blockType === "header-three";

    //show image
    let imageURL = "";
    let imageCaption = "";
    if (blockType === "atomic") {
      const block = contentState.getBlockForKey(currentFocus);
      const entityKey = block.getEntityAt(0);
      if (!entityKey) return null;
      const entity = contentState.getEntity(entityKey);
      const type = entity.getType();
      if (type === "IMAGE") {
        const data = entity.getData();
        imageURL = data.src;
        imageCaption = data.caption;
      }
    }

    //try to get link url
    let linkURL = "";
    if (!selection.isCollapsed()) {
      const startKey = selection.getStartKey();

      const blockWithLinkAtBeginning = contentState.getBlockForKey(startKey);
      const startOffset = editorState.getSelection().getStartOffset();
      const linkKey = blockWithLinkAtBeginning.getEntityAt(startOffset);
      if (linkKey) {
        const linkInstance = Entity.get(linkKey);
        const { url } = linkInstance.getData();
        linkURL = url;
      }
    }

    //show placeholder or not
    let placeholderClassName = "";
    if (!contentState.hasText()) {
      if (
        contentState
          .getBlockMap()
          .first()
          .getType() !== "unstyled"
      ) {
        placeholderClassName = "RichEditor-hidePlaceholder";
      }
    }
    this.setState({
      hasBold,
      hasItalic,
      hasStrikethrough,
      hasUnderline,
      hasHighlight,
      hasBlockquote,
      linkURL,
      hasSubHeading1,
      placeholderClassName,
      editorState,
      imageURL,
      imageCaption
    });
  };
  ///calls props onchange for two way binding

  focus = () => this.editorRef.current.focus(); //focus editor

  //returns content HTML and title
  getValues() {
    const contentState = this.state.editorState.getCurrentContent();
    let html = convertToHTML(exporterConfig)(contentState);
    // console.log(html);
    return {
      title: this.state.title,
      content: html
    };
  }
  componentDidMount() {
    this.setState({
      title: this.props.title
    });
    if (this.props.content) {
      const editorContent = convertFromHTML(this.props.content);
      let contentState = convertFromHTML(importerConfig)(this.props.content);
      const decorator = new CompositeDecorator([
        linkPlugin.decorator
        // localImagePlugin.decorator
      ]);
      this._onChange(EditorState.createWithContent(contentState, decorator));
    }
  }

  handleKeyCommand(command, editorState) {
    if (command === "custom-save") {
      this._handleContextMenu("save");
      return "handled";
    }

    const newState = RichUtils.handleKeyCommand(editorState, command);
    if (newState) {
      this._onChange(newState);
      return "handled";
    }
    return "not-handled";
  }

  /*
  handles a button command by running the command function to get the new
  editor state //Todo clear link state after on add, image and refresh image key
  */
  _handleCommand(e, commandFn, ...[args]) {
    if (e) e.preventDefault(); //checks if e is null
    const newState = commandFn(this.state.editorState, args);
    this._onChange(newState);
    //focus editor
    setTimeout(() => {
      this.focus();
    }, 0);
  }

  _handleDroppedFiles = async (selectionState, files) => {
    try {
      const newState = await onDroppedFiles(
        selectionState,
        files,
        this.state.editorState
      );
      this._onChange(newState);
    } catch (error) {
      message.error(error);
    }
  };

  _handlePastedFiles = async files => {
    for (const file of files) {
      try {
        const newState = await onPastedFiles(file, this.state.editorState);

        this._onChange(newState);
      } catch (error) {
        return message.error(error);
      }
    }
  };
  _handlePastedText = (text, html, editorState) => {
    if (!html) {
      return false;
    } //draft js will handle it
    //if html
    const newState = onPastedText(html, editorState);
    this._onChange(newState);
    return true; //handled
  };

  smileyDropDown = (
    <Menu>
      <Picker
        set="apple"
        onClick={emoji => {
          this._handleCommand(null, _onAddSmiley, { emoji: emoji.native });
        }}
        title="Pick an emoji…"
        emoji="point_up"
      />
    </Menu>
  );

  _onSave = e => {
    e.preventDefault();
    if (!this.props.onSave) return;
    return this.props.onSave();
  };

  _onPublish = e => {
    e.preventDefault();
    if (!this.props.onPublish) return;
    return this.props.onPublish();
  };

  _handleAddLink = e => {
    e.preventDefault();
    const url = this.state.linkURL;
    let path = new RegExp(/^http(s)?:\/\//);
    if (!path.test(url)) {
      return message.error("Links should start with https or http");
    } else {
      this._handleCommand(null, _onAddLink, { url });
    }
    this.setState({ showLinkPopover: false });
  };
  handleFileUpload = {
    name: "image",
    action: `${process.env.API_URL}/resource`,
    onChange: info => {
      if (info.file.status === "done") {
        this.setState({
          imageURL: info.file.response.data.url
        });
      }
    }
  };

  _handleContextMenu = key => {
    if (!this.props.onContextMenuClick)
      return console.error("please provide onContextMenuClick callback");
    this.props.onContextMenuClick(key);
  };

  render() {
    const {
      imageURL,
      imageCaption,
      title,
      imageInputKey,
      linkURL,
      hasBold,
      hasItalic,
      hasUnderline,
      hasStrikethrough,
      hasHighlight,
      isOrderedList,
      isUnorderedList,
      hasBlockquote,
      hasSubHeading1,
      placeholderClassName,
      shouldNavStick
    } = this.state;

    const menu = (
      <Menu
        onMouseDown={e => e.preventDefault()}
        onClick={({ key }) => {
          this._handleCommand(null, _onChangeBlockType, { blockType: key });
          // this._toggleBlockType(null, key);<Menu.Item key="paragraph"> Normal </Menu.Item>
        }}
        selectedKeys={hasSubHeading1 ? ["header-three"] : []}
      >
        <Menu.Item key="header-three"> Sub - heading </Menu.Item>
      </Menu>
    );
    const linkMenuPopover = (
      <div>
        <div>
          <Input
            placeholder="https://example.com"
            value={linkURL}
            onChange={e =>
              this.setState({
                linkURL: e.target.value
              })
            }
          />
        </div>
        <br />
        <Button
          ghost
          type="primary"
          size="small"
          onClick={e => this._handleAddLink(e)}
          style={{
            width: "100%"
          }}
        >
          {" "}
          Done{" "}
        </Button>{" "}
      </div>
    );
    const getContextMenu = () => {
      return (
        <Menu
          onClick={({ key }) => {
            this._handleContextMenu(key);
          }}
        >
          {this.props.contextMenu.map(menu => {
            return <Menu.Item key={menu.key}>{menu.value}</Menu.Item>;
          })}
        </Menu>
      );
    };

    const imageMenuPopover = (
      <div>
        <div>
          <Input
            placeholder="Paste a link"
            value={imageURL}
            onChange={e =>
              this.setState({
                imageURL: e.target.value
              })
            }
          />
        </div>
        <br />
        <Upload
          {...this.handleFileUpload}
          key={imageInputKey}
          accept=".jpg, .png, .gif, .jpeg"
        >
          <Button>
            <Icon type="upload" /> or Click to Upload{" "}
          </Button>{" "}
        </Upload>{" "}
        <TextArea
          style={{
            width: "100%",
            margin: "10px 0",
            resize: "none"
          }}
          name="imageCaption"
          value={imageCaption}
          onChange={e => this.setState({ imageCaption: e.target.value })}
          placeholder="Caption"
        />
        <Button
          ghost
          type="primary"
          size="small"
          onClick={e => {
            let src = this.state.imageURL;
            let caption = this.state.imageCaption;
            if (src)
              this._handleCommand(e, onAddImage, {
                src,
                caption
              });
            this.setState({
              imageURL: "",
              imageCaption: "",
              imageInputKey: Date.now(),
              showImagePopover: false
            });
          }}
          style={{
            width: "100%"
          }}
        >
          Done
        </Button>
      </div>
    );

    return (
      <div>
        <div className="featherContainer">
          <div className="buttonToolbar">
            <Dropdown onMouseDown={e => e.preventDefault()} overlay={menu}>
              <button type="button">
                <Icon type="font-size" /> <Icon type="down" />
              </button>
            </Dropdown>
            <button
              type="button"
              className={hasBold ? "activeButton" : ""}
              onMouseDown={e => this._handleCommand(e, _onBoldClick)}
            >
              {" "}
              <Icon type="bold" />
            </button>
            <button
              type="button"
              className={hasItalic ? "activeButton" : ""}
              onMouseDown={e => this._handleCommand(e, _onItalicClick)}
            >
              {" "}
              <Icon type="italic" />{" "}
            </button>{" "}
            {/*<button type='button' className={hasUnderline? "activeButton":""}  onMouseDown={this._onUnderlineClick.bind(this)}><Icon type="underline" /></button>
                <button type='button' className={hasStrikethrough? "activeButton":""}  onMouseDown={this._onStrikethroughClick.bind(this)}><Icon type="strikethrough" /></button>
                */}{" "}
            <button
              type="button"
              className={hasHighlight ? "activeButton" : ""}
              onMouseDown={e => this._handleCommand(e, _onHighlightClick)}
            >
              {" "}
              <Icon type="highlight" />{" "}
            </button>{" "}
            <button
              type="button"
              onMouseDown={e => this._handleCommand(e, _onBlockquote)}
            >
              {" "}
              <Quote
                fontSize="1em"
                color={hasBlockquote ? "dodgerblue" : "#777"}
              />
            </button>{" "}
            <Dropdown
              onMouseDown={e => e.preventDefault()}
              trigger={["click"]}
              overlay={this.smileyDropDown}
            >
              <button type="button">
                {" "}
                <Icon type="smile" />{" "}
              </button>
            </Dropdown>
            <Popover
              content={linkMenuPopover}
              placement="bottomLeft"
              title="Attach Link"
              trigger="hover"
              visible={this.state.showLinkPopover}
              onVisibleChange={visible => {
                this.setState({ showLinkPopover: visible });
              }}
            >
              <button onMouseDown={e => e.preventDefault()} type="button">
                {" "}
                <Icon type="link" />{" "}
              </button>
            </Popover>
            <Popover
              content={imageMenuPopover}
              placement="bottomLeft"
              title="Attach Image"
              trigger="click"
              visible={this.state.showImagePopover}
              onVisibleChange={visible => {
                this.setState({ showImagePopover: visible });
              }}
            >
              <button
                onMouseDown={e => e.preventDefault()}
                onClick={() => this.setState({ showImagePopover: true })}
                type="button"
              >
                {" "}
                <Icon type="picture" />{" "}
              </button>
            </Popover>
            <button
              type="button"
              onMouseDown={e => this._handleCommand(e, _onUndo)}
            >
              {" "}
              <Icon type="undo" />{" "}
            </button>{" "}
            <button
              type="button"
              onMouseDown={e => this._handleCommand(e, _onRedo)}
            >
              {" "}
              <Icon type="redo" />{" "}
            </button>
            {!this.props.contextMenu && (
              <span style={{ float: "right" }}>
                <Dropdown
                  onMouseDown={e => e.preventDefault()}
                  overlay={getContextMenu()}
                  placement="bottomRight"
                >
                  <button type="button">
                    <Icon type="more" style={{ fontSize: 22 }} />
                  </button>
                </Dropdown>
              </span>
            )}
            <button
              type="button"
              style={{
                display: "inline-flex",
                flexDirection: "column",
                alignItems: "center",
                float: "right"
              }}
              onClick={this._onSave.bind(this)}
            >
              <Icon type="check" /> <span style={{ fontSize: 12 }}>Save</span>
            </button>
            <button
              type="button"
              style={{
                display: "inline-flex",
                flexDirection: "column",
                alignItems: "center",
                float: "right"
              }}
              onClick={this._onPublish.bind(this)}
            >
              <ReleaseIcon /> <span style={{ fontSize: 12 }}>Publish</span>
            </button>
          </div>
          <div className={`editor ${placeholderClassName}`}>
            <TextArea
              className="textArea postTitle"
              style={{
                fontSize: 32,
                width: "100%",
                border: "none",
                resize: "none"
              }}
              name="title"
              placeholder="Title"
              onChange={e => this.setState({ title: e.target.value })}
              value={title}
              autosize
            />

            <Editor
              blockStyleFn={this.getBlockStyle}
              editorState={this.state.editorState}
              handleKeyCommand={this.handleKeyCommand}
              onChange={this._onChange}
              plugins={this.plugins}
              placeholder={"Content goes here"}
              ref={this.editorRef}
              defaultBlockRenderMap={false}
              keyBindingFn={customKeyBindings}
              blockRenderMap={blockRenderMap}
              handleDroppedFiles={this._handleDroppedFiles.bind(this)}
              handlePastedFiles={this._handlePastedFiles.bind(this)}
              handlePastedText={this._handlePastedText.bind(this)}
            />
          </div>
        </div>
        <style jsx>
          {`
            button,
            .button-editor,
            .EmojiSelectButton,
            .draftJsEmojiPlugin__emojiSelectButton__3sPol {
              background: white;
              border: none;
              padding: 5px;
              margin: 10px 8px;
            }

            .button-editor {
              color: blue;
            }
            .activeButton {
              color: dodgerblue;
            }
            .hideInputField {
              display: none !important;
            }

            button:active,
            button:hover,
            button:focus {
              outline-style: none;
              -moz-outline-style: none;
              background-color: #f3f3f3;
              cursor: pointer;
            }

            .featherContainer {
              border: 1px solid #ddd;
              padding: 10px;
              border-radius: 10px;
              margin: 10px 0;
              postion: relative;
            }
            .buttonToolbar {
              border-bottom: 1px solid #ddd;
              margin-bottom: 10px;
              background: white;
              position: relative;
            }
            .editor {
              overflow: auto;
              height: 550px;
              line-height: 2em;
              text-align: justify;
              -webkit-hyphens: auto;
              -moz-hyphens: auto;
              hyphens: auto;
            }
            :global(.RichEditor-hidePlaceholder
                .public-DraftEditorPlaceholder-root) {
              display: none;
            }
            :global(.editor img) {
              display: block;
              max-width: 100%;
              margin: 15px auto;
            }
            :global(.public-DraftEditor-content) {
              max-height: 100%;
              padding: 10px;
            }
            :global(.public-DraftEditorPlaceholder-root) {
              padding: 10px;
            }
            @media only screen and (max-width: 600px) {
              .featherContainer {
                position: fixed;
                overflow: hidden;
                left: 20px;
                right: 20px;
              }
            }
          `}
        </style>
      </div>
    );
  }
}
