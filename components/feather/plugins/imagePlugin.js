import { Fragment, useState } from "react";
import { Input, Icon } from "antd";
import { EditorBlock } from "draft-js";
import { PostImage } from "../../PostImage";

function findImageEntities(contentBlock, callback, contentState) {
  contentBlock.findEntityRanges(character => {
    const entityKey = character.getEntity();
    return (
      entityKey !== null &&
      contentState.getEntity(entityKey).getType() === "IMAGE"
    );
  }, callback);
}
export const Image = props => {
  let {
    block, // eslint-disable-line no-unused-vars
    blockProps, // eslint-disable-line no-unused-vars
    customStyleMap, // eslint-disable-line no-unused-vars
    customStyleFn, // eslint-disable-line no-unused-vars
    blockStyleFn,
    decorator, // eslint-disable-line no-unused-vars
    forceSelection, // eslint-disable-line no-unused-vars
    offsetKey, // eslint-disable-line no-unused-vars
    selection, // eslint-disable-line no-unused-vars
    tree, // eslint-disable-line no-unused-vars
    contentState, // eslint-disable-line no-unused-vars
    style,
    className,
    ...elementProps
  } = props;

  let { src, width, caption } = blockProps.resizeData;
  caption = caption ? caption : "";
  className += " " + blockProps.resizeData.className;
  // <PostImage
  //   src={src}
  //   className={className}
  //   style={style}
  //   title={caption}
  //   elementProps={elementProps}
  // />
  // <Fragment>
  //   <img
  //     src={src}
  //     className={className}
  //     style={{ ...style }}
  //     title={caption}
  //     {...elementProps}
  //   />
  //   <figcaption className="postImageCaption">{caption}</figcaption>
  // </Fragment>
  return (
    <Fragment>
      <img
        src={src}
        className={className}
        style={{ ...style }}
        title={caption}
        {...elementProps}
      />
      <figcaption className="postImageCaption">{caption}</figcaption>
    </Fragment>
  );
};

//why editable false, getEntityAt(0)
const createImagePlugin = (config = {}) => {
  const component = config.decorator ? config.decorator(Image) : Image;
  return {
    blockRendererFn: (block, { getEditorState }) => {
      if (block.getType() === "atomic") {
        const contentState = getEditorState().getCurrentContent();
        const entityKey = block.getEntityAt(0);
        if (!entityKey) return null;
        const entity = contentState.getEntity(entityKey);
        const type = entity.getType();
        if (type === "IMAGE") {
          return {
            component,
            editable: false
          };
        }
      }
      return null;
    },
    decorator: {
      strategy: findImageEntities,
      component: component
    }
  };
};

const imagePlugin = {
  decorator: {
    strategy: findImageEntities,
    component: Image // was this the problem , Change this to component
  }
};
export default createImagePlugin;
