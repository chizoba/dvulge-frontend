import React from "react";
import { RichUtils, KeyBindingUtil, EditorState } from "draft-js";

//link strategy
export const linkStrategy = (contentBlock, callback, contentState) => {
  contentBlock.findEntityRanges(character => {
    const entityKey = character.getEntity();
    return (
      entityKey !== null &&
      contentState.getEntity(entityKey).getType() === "LINK"
    );
  }, callback);
};

//link component : renders anchor tag with url
export const Link = props => {
  const { contentState, entityKey } = props;
  let { url } = contentState.getEntity(entityKey).getData();
  return (
    <a
      className="link"
      href={url}
      rel="noopener noreferrer"
      target="_blank"
      aria-label={url}
    >
      {props.children}
    </a>
  );
};

const addLinkPlugin = {
  // handleKeyCommand(command, editorState, {getEditorState, setEditorState}){
  //   if (command !=="add-link"){
  //     return "not-handled"
  //   }
  //   const url; //get url from input field
  //   if (!url)return 'handled' //we can set to null or just return
  //   const {editorState} = this.state;
  //   const contentState = editorState.getCurrentContent();
  //   //create entity
  //   const contentStateWithEntity = contentState.createEntity(
  //     'LINK',
  //     'MUTABLE',
  //     {url}
  //   )
  //   //updating current content in editor state
  //   const newEditorState = EditorState.push(editorState, contentStateWithEntity,"create-entity");
  //   //set state
  //   const entityKey = contentStateWithEntity.getLastCreatedEntityKey()
  //   setEditorState(RichUtils.toggleLink(newEditorState, newEditorState.getSelection(), entityKey))
  //   return 'handled'
  // },

  decorators: [
    {
      strategy: linkStrategy,
      component: Link
    }
  ],
  decorator: {
    strategy: linkStrategy,
    component: Link
  }
};

export default addLinkPlugin;
