import Immutable from "immutable";
const blockRenderMap = Immutable.Map({
  "header-one": {
    element: "h3"
  },
  "header-two": {
    element: "h3"
  },
  "header-three": {
    element: "h3"
  },
  "unordered-list-item": {
    element: "li"
  },
  // paragraph: {
  //   element: "p"
  // },
  "ordered-list-item": {
    element: "li"
  },
  atomic: {
    element: "figure"
  },
  blockquote: {
    element: "blockquote"
  },
  unstyled: {
    element: "div"
  }
});

export default blockRenderMap;
