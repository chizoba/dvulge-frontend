import { getDefaultKeyBinding, KeyBindingUtil } from "draft-js";
const { hasCommandModifier } = KeyBindingUtil;

function customKeyBindings(e) {
  if (e.keyCode === 83 && hasCommandModifier(e)) {
    //save
    return "custom-save";
  }
  return getDefaultKeyBinding(e);
}
export default customKeyBindings;
