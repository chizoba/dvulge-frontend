import { EditorState, RichUtils, Modifier, AtomicBlockUtils } from "draft-js";

//handle undo / redo
export function _onUndo(editorState) {
  return EditorState.undo(editorState);
}

export function _onRedo(editorState) {
  return EditorState.redo(editorState);
}

export function _onBoldClick(editorState) {
  return RichUtils.toggleInlineStyle(editorState, "BOLD");
}
export function _onItalicClick(editorState) {
  return RichUtils.toggleInlineStyle(editorState, "ITALIC");
}
export function _onUnderlineClick(editorState) {
  return RichUtils.toggleInlineStyle(editorState, "UNDERLINE");
}
export function _onStrikethroughClick(editorState) {
  return RichUtils.toggleInlineStyle(editorState, "STRIKETHROUGH");
}
export function _onHighlightClick(editorState) {
  return RichUtils.toggleInlineStyle(editorState, "HIGHLIGHT");
}
export function _onChangeBlockType(editorState, args) {
  const blockType = args.blockType;
  return RichUtils.toggleBlockType(editorState, blockType);
}
export function _onBlockquote(editorState) {
  return RichUtils.toggleBlockType(editorState, "blockquote");
}

//refresh addlink
export function _onAddLink(editorState, args) {
  const selection = editorState.getSelection();
  const url = args.url;
  if (!url) {
    return RichUtils.toggleLink(editorState, selection, null);
  }

  const content = editorState.getCurrentContent();
  const contentStateWithEntity = content.createEntity("LINK", "MUTABLE", {
    url
  });
  const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
  const newEditorState = EditorState.set(editorState, {
    currentContent: contentStateWithEntity
  });
  return RichUtils.toggleLink(newEditorState, selection, entityKey);
}

export function _onAddSmiley(editorState, args) {
  const emoji = args.emoji;
  const selection = editorState.getSelection(); //get selectopm
  let focusOffset = selection.getFocusOffset() + emoji.length;
  const contentState = editorState.getCurrentContent();
  let nextContentState = EditorState.createEmpty();
  if (selection.isCollapsed()) {
    nextContentState = Modifier.insertText(contentState, selection, emoji);
  } else {
    nextContentState = Modifier.replaceText(contentState, selection, emoji);
  }
  //set cursor position after
  let selectionState = selection.merge({
    anchorOffset: focusOffset + 1,
    focusKey: nextContentState.getFirstBlock().getKey(),
    focusOffset: focusOffset
  });

  let nextEditorState = EditorState.push(
    editorState,
    nextContentState,
    "insert-characters"
  );
  return nextEditorState;
}

//refresh imageURL and inputKEy

export function onAddImage(editorState, args) {
  const src = args.src;
  const caption = args.caption;
  const contentState = editorState.getCurrentContent();
  const contentStateWithEntity = contentState.createEntity(
    "IMAGE",
    "IMMUTABLE",
    {
      src,
      caption,
      className: "imgCenterBlock"
    }
  );
  const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
  const newEditorState = EditorState.set(editorState, {
    currentContent: contentStateWithEntity
  });
  return AtomicBlockUtils.insertAtomicBlock(newEditorState, entityKey, " ");
}
