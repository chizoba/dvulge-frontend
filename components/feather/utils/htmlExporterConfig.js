import { Fragment } from "react";
const exporterConfig = {
  styleToHTML: style => {
    if (style === "HIGHLIGHT") {
      return (
        <span
          style={{
            background: "#fffe0d",
            borderRadius: "3px",
            padding: "0 2px"
          }}
          className="highlight"
        />
      );
    }
  },
  entityToHTML: (entity, originalText) => {
    if (entity.type === "LINK") {
      return (
        <a href={entity.data.url} target="_blank">
          {originalText}
        </a>
      );
    }
    if (entity.type === "link") {
      return (
        <a href={entity.data.url} target="_blank">
          {originalText}
        </a>
      );
    }
    if (entity.type === "IMAGE") {
      let width = entity.data.width || 40;
      return (
        <img
          src={entity.data.src}
          className={entity.data.className}
          width={`${width}%`}
          title={entity.data.caption}
        />
      );
    }
    return originalText;
  }
};

export default exporterConfig;
