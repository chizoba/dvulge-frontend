const importerConfig = {
  htmlToStyle: (nodeName, node, currentStyle) => {
    if (nodeName === "span" && node.className === "highlight") {
      return currentStyle.add("HIGHLIGHT");
    } else {
      return currentStyle;
    }
  },
  htmlToEntity: (nodeName, node, createEntity) => {
    if (nodeName === "img") {
      return createEntity("IMAGE", "IMMUTABLE", {
        src: node.src,
        className: node.className,
        width: node.width,
        caption: node.title
      });
    }

    if (nodeName === "a") {
      return createEntity("LINK", "MUTABLE", {
        url: node.href //do we need the rest
      });
    }
    return null;
  },
  htmlToBlock: (nodeName, node) => {
    if (nodeName === "hr" || nodeName === "img") {
      // "atomic" blocks is how Draft.js structures block-level entities.
      return "atomic";
    }
    if (nodeName === "figcaption") {
    }
    return null;
  }
};

export default importerConfig;
