import { EditorState, RichUtils, Modifier, AtomicBlockUtils } from "draft-js";
import { message } from "antd";
import uploadImageToServer from "./uploadImageToServer";
async function onDroppedFiles(selectionState, files, editorState) {
  for (const file of files) {
    let src;
    try {
      src = await uploadImageToServer(file);
    } catch (error) {
      throw error.message;
    }
    const contentState = editorState.getCurrentContent();
    // let src = URL.createObjectURL(file); //for first image
    let contentStateWithEntity = contentState.createEntity(
      "IMAGE",
      "IMMUTABLE",
      {
        src
        // width: 100 //percent
      }
    );
    const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
    let cs = Modifier.applyEntity(contentState, selectionState, entityKey);
    // const newEditorState = EditorState.set(this.state.editorState, {currentContent:cs})
    // this.onChange(AtomicBlockUtils.insertAtomicBlock(newEditorState, entityKey,''))

    const newEditorState = EditorState.set(editorState, {
      currentContent: cs
    });
    return AtomicBlockUtils.insertAtomicBlock(newEditorState, entityKey, " ");
  }
}

export default onDroppedFiles;
