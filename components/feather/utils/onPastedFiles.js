import { EditorState, RichUtils, Modifier, AtomicBlockUtils } from "draft-js";
import uploadImageToServer from "./uploadImageToServer";

async function onPastedFiles(file, editorState) {
  // for (const file of files) {
  //convert this to function
  let src;
  try {
    src = await uploadImageToServer(file);
  } catch (error) {
    throw error.message;
  }
  const contentState = editorState.getCurrentContent();
  const selectionState = editorState.getSelection();
  let contentStateWithEntity = contentState.createEntity("IMAGE", "IMMUTABLE", {
    src
    // width: 100 //percent
  });
  const entityKey = contentStateWithEntity.getLastCreatedEntityKey();
  let cs = Modifier.applyEntity(contentState, selectionState, entityKey);
  // const newEditorState = EditorState.set(this.state.editorState, {currentContent:cs})
  // this.onChange(AtomicBlockUtils.insertAtomicBlock(newEditorState, entityKey,''))

  const newEditorState = EditorState.set(editorState, {
    currentContent: cs
  });
  return AtomicBlockUtils.insertAtomicBlock(newEditorState, entityKey, " ");
  // }
}

export default onPastedFiles;
