import { EditorState, RichUtils, Modifier, AtomicBlockUtils } from "draft-js";
import { convertFromHTML, convertToHTML } from "draft-convert";
import { stateFromHTML } from "draft-js-import-html";
import importerConfig from "./htmlImporterConfig";
//handles pasted html
function onPastedHTML(html, editorState) {
  // const { editorState } = this.state;

  // const contentState = convertFromHTML(importerConfig)(html);
  // const decorator = new CompositeDecorator([
  //   linkPlugin.decorator,
  //   localImagePlugin.decorator
  // ]);

  // const blockMap = stateFromHTML(html).blockMap;
  const convertedHTML = convertFromHTML(importerConfig)(html);
  const blockMap = convertedHTML.blockMap;

  const newState = Modifier.replaceWithFragment(
    editorState.getCurrentContent(),
    editorState.getSelection(),
    blockMap
  );

  return EditorState.push(editorState, newState, "insert-fragment");
}
export default onPastedHTML;
