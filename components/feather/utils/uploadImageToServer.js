import api from "../../../api";
import { validateImage } from "../../../utils";
const uploadImageToServer = async image => {
  //validate image
  try {
    validateImage(image);
  } catch (error) {
    {
      message: error;
    }
  }
  const formData = new FormData();
  formData.append("image", image);
  let response = await api.upload(formData);
  if (response.error) throw { ...response.error };
  return response.data.url; //return url
};

export default uploadImageToServer;
