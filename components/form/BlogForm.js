import React, { useState, Fragment } from "react";
import {
  Input,
  Button,
  Select,
  Upload,
  Switch,
  Icon,
  Tooltip,
  message,
  Modal
} from "antd";
import api from "../../api";
import { connect } from "react-redux";
const { TextArea } = Input;
const { Option } = Select;
import { Router } from "../../routes";
import imageValidator from "../../utils/imageValidator";
// import slugify from 'slugify'
const { confirm } = Modal;
import debounce from "lodash.debounce";
import ImageInput from "../ImageInput";
import { dataURLToBlob } from "blob-util";
import validDataUrl from "valid-data-url";
import {
  UPDATE_AUTH_USER_SUCCESS,
  SET_PAGE_LOADING
} from "../../store/constants";

class BlogForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        title: "",
        slug: "",
        description: "",
        about: "",
        categories: [],
        logo: ""
      },
      showResetBtn: false,
      fetchError: false,
      errors: {},
      loading: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.deleteBlog = this.deleteBlog.bind(this);
  }

  async componentDidMount() {
    if (this.props.editMode) {
      let response = await api.blog.get(this.props.blog.slug);
      //handle api error
      if (response.error != null) {
        return this.setState({ fetchError: true });
      } //throw error
      //handle success
      let blog = response.data.blog;
      let { title, slug, description, logo, about, categories } = blog;
      let categoriesToString = categories.join(" , ");
      this.setState({
        form: {
          title,
          slug,
          description,
          logo,
          about: about || "",
          categories: categoriesToString
        },
        previewImg: logo
      });
    }
  }

  async handleSubmit(e) {
    e.preventDefault();
    const { editMode } = this.props; //if false this is a new blog being created.
    //clear errors and set loading
    this.setState({ loading: true, errors: {} });
    let formData = new FormData();
    const { form } = this.state;
    for (let field in form) {
      let value;
      if (field === "logo" && validDataUrl(form[field])) {
        value = dataURLToBlob(form[field]);
      } else {
        value = this.state.form[field] || "";
      }
      formData.append(field, value);
    }
    // Submit form
    let response = editMode
      ? await api.blog.update(formData)
      : await api.blog.create(formData);

    this.setState({ loading: false });
    //validate response
    const { error, status, data } = response;
    if (error !== null) {
      switch (status) {
        case 400: //invalid form data
          message.error(response.error.message);
          this.setState({ errors: response.error });
          break;
        case 401:
          message.error(response.error.message);
          break;
        case 403:
          message.error(response.error.message);
          break;
        default:
          message.error("Something went wrong. Try again later");
      }
      return;
    }

    const { _id, slug, title } = data.blog;
    this.props.updateUser({ ...this.props.user, blog: { _id, slug, title } });
    if (!editMode) {
      this.props.setPageLoading();
      return Router.replaceRoute("blog", { slug });
    } else {
      return message.success("Blog updated succesfully");
    }
  }

  validateSlug = debounce(async slug => {
    if (!/^[A-Za-z0-9-]{2,}$/.test(slug)) {
      return this.setState({
        ...this.state,
        errors: {
          ...this.state.errors,
          slug: "Only alphabets, numbers and - allowed. Minimum length is 2"
        }
      });
    }
    const response = await api.blog.checkBlogSlugIsUnique(slug);
    if (response.error !== null && response.status === 400) {
      return this.setState({
        ...this.state,
        errors: {
          ...this.state.errors,
          slug: response.error.slug
        }
      });
    } else if (response.data.exist) {
      //blog exist set error
      return this.setState({
        ...this.state,
        errors: {
          ...this.state.errors,
          slug: "This URL already exist"
        }
      });
    }
  }, 500); //wait time is 500 ms

  deleteBlog = e => {
    e.preventDefault();
    confirm({
      title: "Do you want to delete?",
      content:
        "Deleting this blog would also remove all its posts from our system.",
      onOk: async () => {
        const response = await api.blog.delete();
        if (response.error !== null)
          return message.error("Something went wrong. Try again later.");
        //if successfull. Clear blog from state and redirect to create-blog screent
        await this.props.updateUser({ ...this.props.user, blog: {} });
        return Router.replaceRoute("createBlog");
      },
      onCancel() {}
    });
  };

  handleChange(event) {
    let name = event.target.name;
    let value = event.target.value;
    if (name === "slug" && value) {
      value = value.replace(/\s+/g, "-").toLowerCase();
      this.validateSlug(value);
    }
    this.setState({
      ...this.state,
      form: { ...this.state.form, [name]: value },
      errors: { ...this.state.errors, [name]: "" }
    });
  }

  onDone = logo => {
    const { errors, form } = this.state;
    this.setState({
      form: { ...form, logo },
      errors: { ...errors, avatar: "" }
    });
  };
  onDelete = () => {
    const { errors, form } = this.state;
    this.setState({
      form: { ...form, logo: "" },
      errors: { ...errors, avatar: "" }
    });
  };

  render() {
    let { title, description, slug, about, categories, logo } = this.state.form;
    let { errors, loading } = this.state;
    return (
      <div>
        <div>
          <form onSubmit={this.handleSubmit} className="blogFormContainer">
            <div className="formItem">
              <span className={"formError"}>{errors.title}</span>
              <label>Title</label>
              <Input
                placeholder="Title"
                name="title"
                value={title}
                onChange={this.handleChange}
                style={{ maxWidth: "300px" }}
                required
              />
            </div>
            {!this.props.editMode && ( //only showed during initial creation
              <div className="formItem">
                <span className={"formError"}>{errors.slug}</span>
                <label> Custom URL </label>

                <Tooltip
                  trigger={["focus"]}
                  title="You can not change this after."
                  placement="bottomLeft"
                >
                  <Input
                    addonBefore="dvulge.com/"
                    name="slug"
                    placeholder="my-blog"
                    value={slug}
                    onChange={this.handleChange}
                    style={{ maxWidth: "300px" }}
                    required
                  />
                </Tooltip>
              </div>
            )}
            <div className="formItem">
              <span className={"formError"}>{errors.description}</span>
              <label> Short Description</label>
              <TextArea
                maxLength="160"
                style={{ maxWidth: 300, resize: "none" }}
                name="description"
                placeholder="Give us a short description.."
                value={description}
                onChange={this.handleChange}
                autosize
                required
              />
            </div>
            <div className={"formItem"}>
              <span className={"formError"}>{errors.logo}</span>
              <label> Logo {!this.props.editMode && "(optional)"}</label>
              <ImageInput
                image={logo}
                onDone={this.onDone.bind(this)}
                width={250}
                height={250}
                onError={error =>
                  this.setState({ errors: { ...errors, logo: error } })
                }
                onDelete={this.onDelete.bind(this)}
              />
            </div>
            {this.props.editMode && (
              <Fragment>
                <div className="formItem">
                  <span className={"formError"}>{errors.categories}</span>
                  <label> Categories</label>
                  <Tooltip
                    trigger={["focus"]}
                    title="Separate each category with a comma. You can edit this later"
                    placement="bottomLeft"
                  >
                    <Input
                      placeholder="Food, Lifestyle"
                      name="categories"
                      value={categories}
                      onChange={this.handleChange}
                      style={{ maxWidth: "300px" }}
                    />
                  </Tooltip>
                </div>

                <div className="formItem">
                  <label> About</label>
                  <span className={"formError"}>{errors.about}</span>
                  <Input
                    type="url"
                    placeholder="https://dvulge.com/about-post-url"
                    name="about"
                    value={about}
                    onChange={this.handleChange}
                    style={{ maxWidth: "300px" }}
                  />
                </div>

                <div style={{ fontSize: 14 }}>
                  <div>
                    <a className="danger" onClick={this.deleteBlog}>
                      <Icon type="delete" /> Delete Blog{" "}
                    </a>
                  </div>
                  <br />
                </div>
              </Fragment>
            )}

            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
              style={{ width: "100%", maxWidth: 300, margin: "20px 0" }}
            >
              {loading ? <Icon type="loading" /> : "Save"}
            </Button>
          </form>
        </div>
        <style>
          {`
              form {
                margin: 10px 5px;
                padding: 10px 0;

              }
            `}
        </style>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.payload.user,
    blog: state.auth.payload.user.blog
  };
};

//used after blog creation
const mapDispatchToProps = dispatch => {
  return {
    updateUser: async user =>
      dispatch({ type: UPDATE_AUTH_USER_SUCCESS, payload: user }),
    setPageLoading: () => {
      dispatch({ type: SET_PAGE_LOADING, payload: true });
    }
  };
};
BlogForm.defaultProps = {
  editMode: true //edit mode is true by default
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BlogForm);
