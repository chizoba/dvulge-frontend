import { useState } from "react";
import { Input, Button, Icon, Alert } from "antd";
import api from "../../api";
import { SERVER_ERROR_MSG, LOGIN_SUCCESS_MSG } from "../../utils/constants";

export default function LoginForm() {
  let [email, setEmail] = useState("");
  let [message, setMessage] = useState({});
  let [loading, setLoading] = useState(false);

  const handleSubmit = async e => {
    e.preventDefault();
    setLoading(true);
    let response = await api.user.createAuthLink({ email });
    setLoading(false);
    if (response.error !== null) {
      if (response.status === 400) {
        return setMessage({ type: "error", message: response.error.message });
      }
      return setMessage({ type: "error", message: SERVER_ERROR_MSG });
    }
    //on success set message
    setMessage({ type: "success", message: LOGIN_SUCCESS_MSG });
  };

  return (
    <div>
      <div>
        {Object.keys(message).length > 0 && (
          <Alert message={message.message} type={message.type} showIcon />
        )}
        <br />
        <form onSubmit={handleSubmit}>
          <Input
            style={{
              width: 250,
              display: "inline-block",
              marginRight: 10,
              marginBottom: 10
            }}
            placeholder="Email"
            prefix={<Icon type="mail" style={{ color: "rgba(0,0,0,.25)" }} />}
            onChange={e => {
              setEmail(e.target.value);
            }}
            required
            value={email}
          />
          <Button type="primary" htmlType={"submit"}>
            {loading ? <Icon type="loading" /> : "Submit"}
          </Button>
        </form>
      </div>
    </div>
  );
}
