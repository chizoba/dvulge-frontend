import React from "react";
import ImageInput from "../ImageInput";
import {
  Input,
  Upload,
  Icon,
  Tooltip,
  Modal,
  Button,
  Avatar,
  Checkbox,
  message
} from "antd";
import { connect } from "react-redux";
import api from "../../api";
import { updateAuthUser, deleteAuthUser } from "../../store/actions/auth";
const { TextArea } = Input;
const { confirm } = Modal;
import { Router } from "../../routes";
import { dataURLToBlob } from "blob-util";
import validDataUrl from "valid-data-url";
import {
  UPDATE_AUTH_USER_SUCCESS,
  AUTH_LOGOUT_SUCCESS,
  SET_PAGE_LOADING
} from "../../store/constants";

class ProfileForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      form: {
        fullName: props.profile.fullName,
        bio: props.profile.bio,
        email: props.profile.email,
        avatar: props.profile.avatar,
        twitter: props.profile.twitter,
        facebook: props.profile.facebook,
        instagram: props.profile.instagram
      },
      errors: {},
      loading: false,
      previewImg: props.profile.avatar,
      fileInputKey: Date.now(),
      visible: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.deleteUser = this.deleteUser.bind(this);
  }

  async handleSubmit(e) {
    e.preventDefault();

    this.setState({ loading: true, errors: {} });
    let data = new FormData();
    const { form } = this.state;
    for (let field in form) {
      let value;
      //check if avatar is dataURL and convert to blob
      if (field === "avatar" && validDataUrl(form[field])) {
        value = dataURLToBlob(form[field]);
      } else {
        value = form[field] || "";
      }
      data.append(field, value);
    }
    let { profile } = this.props;
    const response = await api.user.update(data);
    if (response.error !== null) {
      //on error
      const { status, error } = response;
      if (status === 400) {
        this.setState({ errors: error });
        message.error(error.message);
      } else if (status === 403 || status === 401) {
        message.error(error.message);
      } else {
        message.error("Something went wrong");
      }
    } else {
      //on success
      message.success("Profile updated succesfully");
      if (this.props.profile.email !== response.data.user.email)
        this.onChangeEmail();
      this.props.dispatchUpdate(response.data.user);
    }
    return this.setState({ loading: false });
  }

  onChangeEmail() {
    //displays a modal info if user changes email
    Modal.info({
      title: "Confirm New Email",
      content: (
        <div>
          <p>
            Hi, we have updated your email and sent a verification link to your
            inbox.
          </p>
          <p>
            You will need to verfiy that email to make further changes on
            Dvulge.
          </p>
        </div>
      ),
      onOk() {}
    });
  }

  handleChange(event) {
    let name = event.target.name;
    let value = "";
    if (event.target.type === "checkbox") {
      value = event.target.checked;
    } else {
      value = event.target.value;
    }
    this.setState({
      ...this.state,
      errors: { ...this.state.errors, [name]: "" },
      form: { ...this.state.form, [name]: value }
    });
  }

  deleteUser = e => {
    e.preventDefault();
    confirm({
      title: "Do you want to delete?",
      content:
        "This action will remove your profile & blog from our system. If you only want to delete your blog you can do this in the blog settings page.",
      onOk: async () => {
        const response = await api.user.delete();
        if (response.error !== null)
          return message.error("Something went wrong.");
        this.props.dispatchLogout(); //update state
        return Router.replaceRoute("index");
      },
      onCancel() {}
    });
  };

  onDone = avatar => {
    const { form, errors } = this.state;
    this.setState({
      form: { ...form, avatar },
      errors: { ...errors, avatar: "" }
    });
  };
  onDelete = avatar => {
    const { form, errors } = this.state;
    this.setState({
      form: { ...form, avatar: "" },
      errors: { ...errors, avatar: "" }
    });
  };

  handleLogout = async e => {
    e.preventDefault();

    const result = await api.session.delete();
    if (result.error !== null) {
      return message.error(SERVER_ERROR_MSG); //show error
    }
    //dispatch
    this.props.dispatchLogout();
    return Router.pushRoute("index");
  };

  render() {
    const {
      previewImg,
      loading,
      croppedImg,
      fileInputKey,
      crop,
      errors
    } = this.state;
    let {
      fullName,
      bio,
      email,
      avatar,
      twitter,
      facebook,
      instagram
    } = this.state.form;

    return (
      <div>
        <div>
          <form onSubmit={this.handleSubmit}>
            <div className="formItem">
              <label>Avatar </label>
              <span className={"formError"}>{errors.avatar}</span>
              <ImageInput
                image={avatar}
                onDone={this.onDone.bind(this)}
                width={200}
                height={200}
                onError={error =>
                  this.setState({ errors: { ...errors, avatar: error } })
                }
                onDelete={this.onDelete.bind(this)}
              />
            </div>
            <div className="formItem">
              <label>Full Name</label>
              <span className={"formError"}>{errors.fullName}</span>
              <Input
                style={{ maxWidth: "300px" }}
                type="text"
                placeholder="Chizoba Amadi"
                name="fullName"
                value={fullName}
                required={true}
                onChange={this.handleChange}
              />
            </div>
            <div className="formItem">
              <label> Email </label>
              <span className={"formError"}>{errors.email}</span>
              <Input
                style={{ maxWidth: "300px" }}
                type="email"
                placeholder="you@example.com"
                name="email"
                value={email}
                required={true}
                onChange={this.handleChange}
              />
            </div>
            <div className="formItem">
              <label>Bio </label>
              <span className={"formError"}>{errors.bio}</span>
              <TextArea
                maxLength="300"
                style={{ maxWidth: 300, resize: false }}
                name="bio"
                placeholder="About me.."
                value={bio}
                onChange={this.handleChange}
                autosize
              />
            </div>

            <h4>Contact / Social Profile</h4>

            <div className="formItem">
              <span className={"formError"}>{errors.twitter}</span>
              <Input
                style={{ maxWidth: "300px" }}
                placeholder="https:/twitter.com/profile"
                prefix={<Icon type="twitter" />}
                value={twitter}
                name={"twitter"}
                onChange={this.handleChange}
              />
            </div>
            <div className="formItem">
              <span className={"formError"}>{errors.facebook}</span>
              <Input
                style={{ maxWidth: "300px" }}
                placeholder="https:/facebook.com/profile"
                prefix={<Icon type="facebook" />}
                value={facebook}
                name={"facebook"}
                onChange={this.handleChange}
              />
            </div>
            <div className="formItem">
              <span className={"formError"}>{errors.instagram}</span>
              <Input
                style={{ maxWidth: "300px" }}
                placeholder="https:/instagram.com/profile"
                prefix={<Icon type="instagram" />}
                value={instagram}
                name={"instagram"}
                onChange={this.handleChange}
              />
            </div>

            <div style={{ fontSize: 14 }}>
              <div>
                <a className="danger" onClick={this.handleLogout}>
                  <Icon type="logout" /> Logout
                </a>
              </div>
              <br />
            </div>

            <div style={{ fontSize: 14 }}>
              <div>
                <a className="danger" onClick={this.deleteUser}>
                  <Icon type="delete" /> Delete Account{" "}
                </a>
              </div>
              <br />
            </div>

            <Button
              type="primary"
              htmlType="submit"
              className="login-form-button"
              style={{ width: "100%", maxWidth: 300, margin: "20px 0" }}
            >
              {loading ? <Icon type="loading" /> : "Save"}
            </Button>
          </form>
        </div>
        <style>
          {`
          form {
            margin: 10px 0;
            padding: 10px 0;
          }
            `}
        </style>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    dispatchUpdate: user =>
      dispatch({ type: UPDATE_AUTH_USER_SUCCESS, payload: user }),
    dispatchLogout: () => {
      dispatch({ type: SET_PAGE_LOADING, payload: true });
      dispatch({ type: AUTH_LOGOUT_SUCCESS, payload: null });
    }
  };
};
const mapStateToProps = state => {
  return {
    profile: state.auth.payload.user,
    auth: state.auth
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileForm);
