import { useState } from "react";
import { Input, Icon, Button, Card, Modal, Carousel } from "antd";
import api from "../../api";
import { SERVER_ERROR_MSG, LOGIN_SUCCESS_MSG } from "../../utils/constants";

function RegistrationForm() {
  //states
  let [fullName, setFullName] = useState("");
  let [email, setEmail] = useState("");
  let [loading, setLoading] = useState(false);

  //handles blog submission
  const handleSubmit = async e => {
    setLoading(true);
    e.preventDefault();
    let data = {
      fullName,
      email
    };
    let res = await api.user.create(data);
    setLoading(false);
    if (res.error !== null) {
      if (res.status === 400) {
        return onError(
          <div>
            <p> {res.error.message} </p>
            <p> {res.error.fullName} </p>
            <p> {res.error.email} </p>
          </div>
        );
      }
      return onError(SERVER_ERROR_MSG); //else server error
    } else {
      onSuccess();
    }
  };

  //display custom message
  const onSuccess = () => {
    Modal.success({
      title: "Confirmation",
      content: (
        <div>
          <p> {LOGIN_SUCCESS_MSG} </p>
        </div>
      )
    });
  };

  const onError = msg => {
    Modal.error({
      title: "Error",
      content: msg
    });
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <Input
          placeholder="Full Name"
          style={{
            width: 200,
            display: "inline-block",
            marginRight: 10,
            marginBottom: 10
          }}
          prefix={
            <Icon
              type="user"
              style={{
                color: "rgba(0,0,0,.25)"
              }}
            />
          }
          required
          onChange={e => {
            setFullName(e.target.value);
          }}
          value={fullName}
        />
        <Input
          style={{
            width: 200,
            display: "inline-block",
            marginRight: 10,
            marginBottom: 10
          }}
          placeholder="Email"
          prefix={
            <Icon
              type="mail"
              style={{
                color: "rgba(0,0,0,.25)"
              }}
            />
          }
          required
          type="email"
          onChange={e => {
            setEmail(e.target.value);
          }}
          value={email}
        />
        <Button htmlType="submit" className="signUpFormButton">
          {loading ? (
            <Icon type="loading" />
          ) : (
            <span>
              Join <Icon type="arrow-right" />{" "}
            </span>
          )}
        </Button>
      </form>
      <style jsx>
        {`
          .signUpFormButton {
            display: block;
            margin: auto;
          }
          @media only screen and (min-width: 600px) {
            .signUpFormButton {
              display: inline-block;
            }
          }
        `}
      </style>
    </div>
  );
}

export default RegistrationForm;
