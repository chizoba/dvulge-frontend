import React from "react";
import { initGA, logPageView } from "../utils/analytics";
// Require Editor CSS files.

import "antd/dist/antd.css";

export default class Layout extends React.Component {
  componentDidMount() {
    if (!window.GA_INITIALIZED) {
      initGA();
      window.GA_INITIALIZED = true;
    }
    logPageView();
  }
  render() {
    return (
      <div className="page-layout">
        {this.props.children}
        <style jsx global>
          {`
            @import url("https://fonts.googleapis.com/css?family=IBM+Plex+Sans:300,400,700&display=swap");
            * {
              margin: 0;
              box-sizing: border-box;
            }
            body {
              padding: 70px 20px;
              padding-top: 70px;
              padding-bottom: 40px !important;
              font-size: 18px !important;
              font-family: "Quicksand", sans-serif;
              font-family: "IBM Plex Sans", sans-serif;
              font-weight: 400;
            }
            blockquote {
              border-left: 3px solid #ccc;
              // margin:1.5em 10px;
              padding: 5px 30px;
              font-family: "Lato", sans-serif !important;
              font-style: italic;
              font-weight: 700;
            }
            .postImageCaption {
              display: block;
              text-align: center;
              color: #737373;
              font-size: 14px;
            }
            label {
              display: block;
              margin: 3px 0;
              font-size: 14px;
              color: #777;
            }

            select {
              border: none;
              padding: 5px 10px;
              font-size: 14px;
              display: inline-block;
              background: white;
              -webkit-appearance: none;
              appearance: none;
              outline: none;
            }

            .formItem {
              display: block !important;
              margin-bottom: 30px;
            }

            a {
              text-decoration: none;
              color: inherit;
              color: dodgerblue;
              display: inline-block;
            }
            .imgCenterBlock {
              margin: 15px auto;
              display: block;
            }
            .link {
              color: dodgerblue;
            }
            .link:hover {
              cursor: pointer;
            }
            .danger {
              color: red;
            }
            .select {
              border: 1px solid #ccc;
              display: inline-block;
              margin: 5px;
              padding: 0 5px;
            }
            .button {
              padding: 5px 15px;
              margin: 10px 0;
              margin-right: 10px;
              letter-spacing: 0.1em;
              display: inline-block;
              font-size: 14px;
              border: 2px solid #aaa;
              background: white;
            }
            button {
              margin-right: 10px;
            }
            .formError {
              font-size: 14px;
              color: red;
              margin: 3px 0;
              display: block;
            }
            a:hover {
              cursor: pointer;
            }
            .postContent p {
              margin-bottom: 0 !important; //remove ant-d bottom styling from content
            }
            @media only screen and (max-width: 600px) {
              .imgCenterBlock {
                width: 100% !important;
              }
            }
          `}
        </style>
      </div>
    );
  }
}
