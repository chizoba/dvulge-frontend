import React, { useState, Component, Fragment } from "react";
import { connect } from "react-redux";
import { Icon, Input, Avatar, Modal, Menu, Dropdown, message } from "antd";
import LoginForm from "./form/LoginForm";
import api from "../api";
// import Link from "next/link";
// import Router from "next/router";
import { Link, Router } from "../routes";
const { Search } = Input;

import { SERVER_ERROR_MSG } from "../utils/constants";
import ErrorPage from "../pages/_error.js";
import SyncLoader from "react-spinners/SyncLoader";

class Navbar extends Component {
  state = {
    displayMenu: true,
    showLogin: false,
    hasError: false,
    error: null,
    errorInfo: null
  };

  handleMenuChange = menu => {
    const { setActiveMenu, setPageLoading, user } = this.props;
    setPageLoading(true);
    if (menu === "my-blog") {
      Router.pushRoute("blog", { slug: user.blog.slug });
    }
    if (menu === "new-post") {
      Router.pushRoute("compose");
    }
    if (menu === "my-posts") {
      Router.pushRoute("myposts");
    }
    if (menu === "analytics") {
      Router.pushRoute("analytics");
    }
    if (menu === "settings") {
      Router.pushRoute("settings");
    }
  };

  componentDidCatch(error, errorInfo) {
    this.setState({ hasError: true, error, errorInfo });
  }

  render() {
    const {
      isAuthenticated,
      user,
      activeMenu,
      setActiveMenu,
      showMenu,
      dispatchToggleMenu,
      pageLoading
    } = this.props;
    const { showLogin, hasError } = this.state;
    const hasBlog = !!user.blog && !!user.blog.slug;
    return (
      <div>
        <div>
          <div className="navbarContainer">
            <Link route="index">
              <a className="logo">
                <strong>DVULGE</strong>
              </a>
            </Link>
            <div>
              {isAuthenticated ? (
                !hasBlog ? (
                  <a
                    onClick={e => {
                      e.preventDefault();
                      this.props.setPageLoading(true);
                      Router.pushRoute("createBlog");
                    }}
                  >
                    <Icon type="edit" /> Create your blog
                  </a>
                ) : (
                  <a style={{ color: "rgb(224, 36, 94)", marginLeft: 15 }}>
                    <Avatar
                      style={{ color: "#f56a00", backgroundColor: "#fde3cf" }}
                    >
                      {user.fullName[0]}
                    </Avatar>
                  </a>
                )
              ) : (
                <a
                  onClick={e => {
                    e.preventDefault();
                    this.setState({ showLogin: true });
                  }}
                  style={{ color: "rgb(224, 36, 94)" }}
                >
                  <Icon type="login" /> Login
                </a>
              )}
            </div>
            <div
              className={
                showMenu ? "menuToolbar menuToolbarActive" : "menuToolbar"
              }
              style={{ display: hasBlog ? "flex" : "none" }}
            >
              {!showMenu ? (
                <NavItem
                  icon="menu-unfold"
                  iconText={"Menu"}
                  onClick={() => dispatchToggleMenu(true)}
                />
              ) : (
                <Fragment>
                  {/*<NavItem
                    icon="menu-fold"
                    iconText={"Hide"}

                    onClick={() => dispatchToggleMenu(false)}
                  />*/}
                  <NavItem
                    icon="home"
                    title={"My Blog"}
                    active={activeMenu === "my-blog"}
                    onClick={() => this.handleMenuChange("my-blog")}
                  />
                  <NavItem
                    icon="form"
                    title={"New Post"}
                    active={activeMenu === "new-post"}
                    onClick={() => this.handleMenuChange("new-post")}
                  />
                  <NavItem
                    icon="folder-open"
                    title={"My Posts"}
                    active={activeMenu === "my-posts"}
                    onClick={() => this.handleMenuChange("my-posts")}
                  />
                  <NavItem
                    icon="bar-chart"
                    title={"Analytics"}
                    active={activeMenu === "analytics"}
                    onClick={() => this.handleMenuChange("analytics")}
                  />
                  <NavItem
                    icon="setting"
                    title={"Settings"}
                    active={activeMenu === "settings"}
                    onClick={() => this.handleMenuChange("settings")}
                  />
                </Fragment>
              )}
            </div>
            <Modal
              title="Welcome back"
              centered
              visible={showLogin}
              footer={null}
              bodyStyle={{ height: 300 }}
              onCancel={() => this.setState({ showLogin: false })}
            >
              <div style={{ textAlign: "center" }}>
                <h2>
                  <strong>Sign in to your Account</strong>
                </h2>
                <LoginForm />
                <br />
                <span>or </span>
                <Link route="index">
                  <a className="link"> Create your blog's account</a>
                </Link>
              </div>
            </Modal>
          </div>
          <div>
            {hasError ? (
              <ErrorPage />
            ) : pageLoading ? (
              <div
                style={{
                  width: "100%",
                  height: "70vh",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <SyncLoader size={10} color={"dodgerblue"} />
              </div>
            ) : (
              <div className="pageContainer">{this.props.children}</div>
            )}
          </div>
        </div>
        <style jsx>
          {`
            @import url("https://fonts.googleapis.com/css?family=Monoton|Montserrat+Alternates|Playfair+Display:700&display=swap");
            .navbarContainer {
              display: flex;
              justify-content: space-between;
              align-items: center;
              border-bottom: 1px solid #ccc;
              padding: 10px 20px;
              margin-bottom: 20px;
              position: fixed;
              background: white;
              right: 0;
              top: 0;
              left: 0;
              height: 60px;
              font-size: 14px;
              z-index: 500;
            }
            .menuToolbar {
              position: fixed;
              background: white;
              left: 0;
              z-index: 500;
              display: flex;
            }
            .menuToolbarActive {
              bottom: 0;
            }
            .logo {
              color: black;
            }

            @media only screen and (min-width: 450px) {
              .pageContainer {
                padding-left: 60px;
              }
              .menuToolbar {
                top: 60px;
                flex-direction: column;
                border-right: 1px solid #f1f1f1;
              }
            }
            @media only screen and (max-width: 450px) {
              .menuToolbar {
                position: fixed;
                background: white;

                bottom: 0;
                left: 0;
                right: 0;
                z-index: 500;
                display: flex;
                flex-direction: row;
                justify-content: space-between;
                font-size: 12px !important;
                border-top: 1px solid #f1f1f1;
              }
              // .menuToolbarActive {
              //   bottom: 0;
              // }
            }
            // @media only screen and (min-width: 850px) {
            //   .pageContainer {
            //     padding-left: 3px;
            //   }
            // }
          `}
        </style>
      </div>
    );
  }
}

function NavItem({ icon, title, iconText, active, onClick }) {
  const [hover, setHover] = useState(false);
  const handleClick = () => {
    if (onClick) onClick();
    return;
  };
  return (
    <div>
      <div
        style={{
          textAlign: "center",
          padding: 10,
          borderBottom: "1px solid #f5f5f5",
          background: active ? "#e6f7ff" : "inherit",
          color: active ? "dodgerblue" : hover ? "dodgerblue" : "inherit",
          cursor: "pointer"
        }}
        onClick={handleClick}
        onMouseEnter={() => setHover(true)}
        onMouseLeave={() => setHover(false)}
      >
        <span>
          <Icon type={icon} style={{ fontSize: 16, marginBottom: 10 }} />{" "}
          {iconText}
        </span>
        {title && <p> {title} </p>}
      </div>
    </div>
  );
}
const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.payload.isAuthenticated,
    user: state.auth.payload.user,
    authError: state.auth.error,
    activeMenu: state.nav.active,
    showMenu: state.nav.showMenu,
    pageLoading: state.main.pageLoading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setPageLoading: activeNav => {
      dispatch({ type: "SET_PAGE_LOADING", payload: true });
      // dispatch({ type: "SET_ACTIVE_NAV", payload: activeNav });
    },
    dispatchToggleMenu: value => {
      dispatch({ type: "TOGGLE_MENU", payload: value });
      // dispatch({ type: "SET_ACTIVE_NAV", payload: activeNav });
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Navbar);
// export default Navbar
