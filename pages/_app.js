import React from "react";
import App, { Container } from "next/app";
import Layout from "../components/layout";
import Navbar from "../components/navbar";
import { Provider } from "react-redux";
import withRedux from "next-redux-wrapper";
import ErrorPage from "./_error.js";
import initializeStore from "../store";

class MyApp extends App {
  state = {
    //for react error boundaries
    hasError: false,
    error: null,
    errorInfo: null
  };
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }
    //added to handle error
    if (pageProps.statusCode && ctx.res) {
      ctx.res.statusCode = pageProps.statusCode;
    }
    return { pageProps };
  }

  componentDidCatch(error, errorInfo) {
    this.setState({ hasError: true, error, errorInfo });
  }

  render() {
    const { Component, pageProps, store, statusCode } = this.props;
    if (this.state.hasError) {
      return <ErrorPage />;
    }

    return (
      <Container>
        <Provider store={store}>
          <Layout>
            <Navbar>
              {pageProps.statusCode ? (
                <ErrorPage statusCode={pageProps.statusCode} />
              ) : (
                <Component {...pageProps} />
              )}
            </Navbar>
          </Layout>
        </Provider>
      </Container>
    );
  }
}

export default withRedux(initializeStore)(MyApp);
