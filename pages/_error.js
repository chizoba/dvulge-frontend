import React from "react";
import { SET_PAGE_LOADING } from "../store/constants";

class Error extends React.Component {
  static getInitialProps({ res, err, store }) {
    store.dispatch({ type: SET_PAGE_LOADING, payload: false });
    const statusCode = res ? res.statusCode : err ? err.statusCode : null;
    return { statusCode };
  }

  render() {
    return (
      <div className="errorContainer">
        {this.props.statusCode === 404 ? (
          <p>Hi, we can't find the page you are looking for.</p>
        ) : (
          <p>
            {" "}
            Something went wrong. <br /> Please try again later.
          </p>
        )}
        <style jsx>
          {`
            .errorContainer {
              font-size: 26px;
              text-align: center;
              font-weight: bold;
              height: 80vh;
              display: flex;
              justify-content: center;
              align-items: center;
            }
          `}
        </style>
      </div>
    );
  }
}

export default Error;
