import api from "../api";
import {
  Head,
  Meta,
  BlogHeader,
  Card,
  ProfileCard,
  withAuth
} from "../components";
import { getEditor, getBlogURL } from "../utils";
import "antd/dist/antd.css";
import { Input, Menu, Icon, List, Avatar, Empty, Button } from "antd";
import { useState, useEffect, useRef, Component } from "react";
// import Router from "next/router";
import querystring from "query-string";
const { Search } = Input;
const { SubMenu } = Menu;
import { Router } from "../routes";
import { NextSeo } from "next-seo";
import { SET_PAGE_LOADING } from "../store/constants";

function Blog(props) {
  //props
  const { blog, category } = props;
  //state
  const [posts, setPosts] = useState([]);
  const [search, setSearch] = useState(props.search); //we need to control the search input box
  const [hasMorePosts, setHasMorePosts] = useState(false);
  const [postFetchError, setPostFetchError] = useState(false);
  const [editor, setEditor] = useState({});
  const [page, setPage] = useState(1);
  //constant variables
  const selectedKey = category ? category : "default_home";
  const errorMessage = postFetchError
    ? "Could not load posts."
    : "No Posts Found";

  //on first mount and any time editor changes
  useEffect(() => {
    getEditor(blog.editor).then(editor => setEditor(editor || {}));
  }, [props.blog.slug]);

  //on prop changes to blog, category or search reload post list
  useEffect(() => {
    loadPosts();
    if (props.search !== search) setSearch(props.search);
  }, [props.blog.slug, props.search, props.category]);

  const pushRoute = query => {
    //injects new route that updates props
    if (search && !query.hasOwnProperty("search")) query.search = search;
    if (category && !query.hasOwnProperty("category"))
      query.category = category;
    //add blog slug
    query.slug = blog.slug;
    Router.pushRoute("blog", query);
  };

  //load posts based on props
  const loadPosts = async (more = false) => {
    let nextPage = more ? page + 1 : 1; //if user is trying to load more posts
    //get query
    let response = await api.post.list({
      blog: blog.slug,
      published: true,
      page: nextPage,
      search: props.search, //always use the props value. state value may not be updated immediately after the browser histroy button is used.
      category
    });
    //handle error
    if (response.error !== null) {
      setPostFetchError(true);
      setPosts([]);
      setHasMorePosts(false);
      return;
    }
    //on success
    let { numOfPages } = response.data;
    let morePostsAvailable = nextPage < numOfPages;
    more
      ? setPosts([...posts, ...response.data.posts])
      : setPosts([...response.data.posts]);

    setPage(nextPage);
    setHasMorePosts(morePostsAvailable);
  };

  //blog catgories
  let blogCategories = <Menu.Item key={"default_empty"}> - </Menu.Item>;
  if (blog.categories && blog.categories.length) {
    blogCategories = blog.categories.map((category, index) => {
      return (
        <Menu.Item key={category} style={{ textTransform: "capitalize" }}>
          {category}
        </Menu.Item>
      );
    });
  }
  //handleMenuChange
  //handles the selection of a menu item
  const handleMenuChange = selectedKey => {
    let defaultKeys = ["default_home", "default_about", "default_empty"];
    if (!defaultKeys.includes(selectedKey)) {
      pushRoute({ search: "", category: selectedKey });
    }
  };
  //postcards
  const postCards = posts.map((post, index) => {
    return (
      <article key={index}>
        <Card
          title={post.title}
          image={post.thumbnail}
          blogSlug={blog.slug}
          postSlug={post.slug}
          datePublished={post.datePublished}
        />
      </article>
    );
  });

  return (
    <div>
      <Meta
        title={blog.title}
        desc={blog.description}
        allowIndex={true}
        image={blog.logo}
        url={getBlogURL(blog.slug)}
      />

      <div className="blogContainer">
        <BlogHeader
          title={blog.title}
          logo={blog.logo}
          description={blog.description}
          editor={editor}
        />

        <div className="blogBody">
          <Menu
            mode="horizontal"
            selectedKeys={[selectedKey]}
            onClick={({ key }) => {
              handleMenuChange(key);
            }}
          >
            <Menu.Item key="default_home">
              <a href={`/${blog.slug}`}>
                <Icon type="home" /> Home
              </a>
            </Menu.Item>
            <SubMenu
              title={
                <span className="submenu-title-wrapper">
                  <Icon type="appstore" />
                  Menu
                </span>
              }
            >
              {blogCategories}
            </SubMenu>
            {blog.about && (
              <Menu.Item key="default_about">
                <a href={blog.about}>
                  <Icon type="info-circle" /> About
                </a>
              </Menu.Item>
            )}
          </Menu>

          <div style={{ marginTop: 20 }}>
            {category ? (
              <h2 style={{ textTransform: "capitalize" }}>
                <strong>{category}</strong>
              </h2>
            ) : (
              <Search
                placeholder={"search"}
                style={{ maxWidth: 340 }}
                value={search}
                onChange={e => {
                  setSearch(e.target.value);
                }}
                onSearch={value => pushRoute({ search: value })}
              />
            )}
          </div>

          {posts.length > 0 ? (
            <section className="blogPosts">{postCards}</section>
          ) : (
            <div style={{ marginTop: 80 }}>
              <Empty
                image={Empty.PRESENTED_IMAGE_SIMPLE}
                description={<span>{errorMessage}</span>}
              />
            </div>
          )}

          {posts.length > 0 && hasMorePosts && (
            <div style={{ marginLeft: 20 }}>
              <Button
                ghost
                type="primary"
                onClick={e => {
                  e.preventDefault();
                  loadPosts(true);
                }}
              >
                Load more
              </Button>
            </div>
          )}
        </div>
      </div>

      <style jsx>
        {`
          .blogContainer {
            margin: 0 auto;
            margin-bottom: 40px;
          }
          .blogBody {
            margin: 10px auto;
            max-width: 950px;
          }

          .blogPosts {
            margin: 30px auto;
            display: flex;
            justify-content: center;
            flex-wrap: wrap;
          }

          @media only screen and (min-width: 600px) {
            .blogPosts {
              justify-content: flex-start;
            }
          }
        `}
      </style>
    </div>
  );
}

Blog.getInitialProps = async ({ req, query, store }) => {
  let { slug, search, category } = query;
  const { user, isAuthenticated } = store.getState().auth.payload;
  if (isAuthenticated && user.blog && user.blog.slug === slug)
    store.dispatch({ type: "SET_ACTIVE_NAV", payload: "my-blog" });
  else {
    store.dispatch({ type: "SET_ACTIVE_NAV", payload: "" });
  }
  search = search ? search : "";
  category = category ? category : "";
  let response = await api.blog.get(slug);
  if (response.error !== null) return { statusCode: response.status };
  let props = {
    blog: response.data.blog,
    search,
    category
  };
  store.dispatch({ type: SET_PAGE_LOADING, payload: false });
  return props;
};

export default withAuth(Blog);
