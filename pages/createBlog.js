import { withAuth, Head, BlogForm } from "../components";
import { NextSeo } from "next-seo";
import { SET_PAGE_LOADING } from "../store/constants";

function CreateBlog(props) {
  return (
    <div>
      <NextSeo
        title={"Dvulge - Create Blog"}
        description={"Create your blog. Start your journey."}
        allowIndex={true}
        openGraph={{
          title: "Dvulge - Create Blog",
          type: "website",
          description: "Create your blog. Start your journey."
        }}
      />

      <div className="createBlogContainer">
        <h3 style={{ textAlign: "center" }}>
          <strong>Create your blog</strong>
          <p>The Journey starts here</p>
        </h3>
        <div className={"createBlogFormWrapper"}>
          <div className="createBlogImgContainer">
            <img src="https://cdn.dvulge.com/static/create-blog.jpg" />
          </div>
          <div style={{ maxWidth: 300 }}>
            <BlogForm editMode={false} />
          </div>
        </div>
      </div>
      <style>
        {`
                  .createBlogContainer{
                    margin: 20px 0;
                  }
                   .createBlogFormWrapper{
                    max-width: 1000px;
                    display:flex;
                    justify-content:center;
                    flex-wrap:wrap;
                     }
                   .createBlogImgContainer {
                     max-width:500px;
                     margin: 0 20px;
                   }
                   .createBlogImgContainer img{
                     max-width:100%;
                   }
                `}
      </style>
    </div>
  );
}
//ToDo:only allow users with blogs
CreateBlog.getInitialProps = ({ res, req, store }) => {
  if (req) {
    let user = store.getState().auth.payload.user;
    if (user.blog && user.blog.slug) return res.redirect(`/${user.blog.slug}`);
  }
  store.dispatch({ type: SET_PAGE_LOADING, payload: false });
  return {};
};
export default withAuth(CreateBlog, true); //require
