import { Tabs, Radio } from "antd";
import { Collapse } from "antd";

const { Panel } = Collapse;

const { TabPane } = Tabs;

export default function Guide(props) {
  return (
    <div>
      <div className="guideContainer">
        <h3>Hi, Let us answer your question. </h3>

        <p>Select a Topic for relevant guides.</p>

        <div className="guideTabContainer">
          <Tabs defaultActiveKey="1" tabPosition={"top"}>
            <TabPane tab="Profile" key={1}>
              <Collapse bordered={false} defaultActiveKey={["0"]}>
                <p> Creating</p>
                <Panel header="Logging in" key="1"></Panel>
                <Panel header="Editing your profile" key="2"></Panel>
                <Panel
                  header="Displaying your profile on your blog"
                  key="3"
                ></Panel>
                <Panel header="Deleting your profile" key="4"></Panel>
              </Collapse>
            </TabPane>
            <TabPane tab="Blog" key={2}>
              <p>
                {" "}
                Every blog on Dvulge is linked to a user's account. A blog is
                identified by a unique slug chosen by the account holder.
                <a>www.dvulge.com/slug</a> .
              </p>
              <p> Once</p>
              <p>
                <strong>Common Topics</strong>{" "}
              </p>
              <Collapse bordered={false} defaultActiveKey={["0"]}>
                <Panel header="Customizing your blog" key="1">
                  <p>
                    Click on your name icon in the top-right corner. Select{" "}
                    <a>Edit blog & profile </a>
                  </p>
                  <p>Select the blog tab</p>
                  The following information can be edited
                  <ul>
                    <li>Title: The name of your blog.</li>
                    <li>Logo: Recommended size 500 x 500.</li>
                    <li>Short Desciption: A bio for your blog.</li>
                    <li>
                      Categories: A flat list of sections your post can be
                      linked to. See <a>How to add a post to category </a>.{" "}
                    </li>
                    <li>
                      About: A link to your about page. See{" "}
                      <a>Creating an about page</a>.
                    </li>
                  </ul>
                </Panel>
                <Panel header="Creating an About page" key="2">
                  <p>You can create an about page by following these steps:</p>
                  <ul>
                    <li>
                      {" "}
                      Click on your name icon in the top-right corner. Select{" "}
                      <a>Posts & Drafts</a>{" "}
                    </li>
                    <li>
                      {" "}
                      Click on <a>Create new post</a> .{" "}
                    </li>
                    <li>
                      {" "}
                      Edit and save this post. Copy the link in your URL bar
                    </li>
                    <li>
                      {" "}
                      Click on your name icon in the top-right corner. Select{" "}
                      <a>Edit blog & profile </a>
                    </li>
                    <li>Paste the copied link in the about field</li>
                  </ul>
                  <p>An about menu will be displayed on your blog's page. </p>
                  <p>
                    You can choose to publish or leave the created post as a
                    draft.
                  </p>
                </Panel>
                <Panel header="Understanding Analytics" key="3"></Panel>
                <Panel header="Changing your slug/Domain name " key="4">
                  <p>
                    It is not possible to change your blog's slug after
                    creation. <br /> This is because it is tied to your
                    analytics account.
                  </p>
                  <p>
                    Dvulge does not allow linking domain name's to a blog
                    account.
                  </p>
                </Panel>
                <Panel header="Receving Payments" key="5">
                  <p>
                    It is not currently possible to receive payments. However,
                    this feature is in progress.{" "}
                  </p>
                </Panel>
                <Panel header="Creating multiple blogs" key="6"></Panel>
                <Panel header="Deleting your blog" key="7"></Panel>
              </Collapse>
            </TabPane>
            <TabPane tab="Post" key={3}>
              Content of tab
              <Panel header="Editing your post" key="1"></Panel>
              <Panel header="Publishing your post" key="2"></Panel>
              <Panel
                header="Viewing and Understanding analytics"
                key="3"
              ></Panel>
              <Panel header="Deleting your post" key="4"></Panel>
            </TabPane>
          </Tabs>
        </div>
      </div>
      <style jsx>
        {`
          .guideContainer {
            max-width: 700px;
            margin: auto;
          }
          .guideTabContainer {
            margin: 40px 0;
          }
        `}
      </style>
    </div>
  );
}
