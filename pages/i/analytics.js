import "antd/dist/antd.css";
import { withAuth, SectionLoader, Head } from "../../components";
import React from "react";
import { connect } from "react-redux";
import api from "../../api";
import { Line } from "react-chartjs-2";
import { Statistic, Row, Col, Icon, DatePicker, message } from "antd";
import { Tabs, Select } from "antd";
const { MonthPicker } = DatePicker;
const { TabPane } = Tabs;
import moment from "moment";
import { NextSeo } from "next-seo";
import { SET_PAGE_LOADING } from "../../store/constants";

const sumArray = (total, num) => {
  total = parseInt(total, 10);
  num = parseInt(num, 10);
  return total + num;
};

const sumFloats = (total, num) => {
  total = parseFloat(total);
  num = parseFloat(num);
  return total + num;
};

const getTopPosts = async () => {};
class Analytics extends React.Component {
  static getInitialProps({ store }) {
    store.dispatch({ type: "SET_ACTIVE_NAV", payload: "analytics" });
    store.dispatch({ type: SET_PAGE_LOADING, payload: false });
  }
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      days: [],
      sessions: [],
      pageviews: [],
      total: {},
      errors: {},
      period: moment(),
      loading: false
    };
    this.getData = this.getData.bind(this);
    this.getAnalytics = this.getAnalytics.bind(this);
    this.handlePeriodChange = this.handlePeriodChange.bind(this);
    this.getStatisticsChart = this.getStatisticsChart.bind(this);
  }

  async componentDidMount() {
    await this.getAnalytics();
  }

  async getAnalytics() {
    this.setState({ loading: true });
    let { period } = this.state; //period is a moment object
    let { blog } = this.props.user;
    let response = await api.analytics.getBlog(blog._id, period.toDate());
    if (response.error !== null) {
      //handle error
      this.setState({ loading: false });
      return message.error("Could not load analytics");
    }
    if (!response.data.analytics) {
      this.setState({
        users: [],
        days: [],
        sessions: [],
        pageviews: [],
        total: {},
        errors: {},
        loading: false
      });

      return message.info("You have no analytics for this period");
    }
    let { analytics } = response.data;

    let { users, sessions, pageviews, bounceRates, days } = analytics;
    this.setState({
      users,
      days,
      sessions,
      pageviews,
      bounceRates,
      loading: false
    });
  }
  getData = (data, title, color) => {
    return {
      labels: this.state.days,
      datasets: [
        {
          label: title,
          fill: false,
          borderColor: color,
          backgroundColor: color,
          data: data
        }
      ]
    };
  };

  getStatisticsChart = (data, title, color) => {
    return (
      <Line
        data={this.getData(data, title, color)}
        options={{
          scales: {
            yAxes: [
              {
                gridLines: {
                  lineWidth: 0
                },
                ticks: {
                  beginAtZero: true,
                  callback: function(value) {
                    if (value % 1 === 0) {
                      return value;
                    }
                  }
                }
              }
            ]
          }
        }}
      />
    );
  };

  handlePeriodChange(date, dateString) {
    this.setState({ period: date }, this.getAnalytics);
  }

  render() {
    let { users, sessions, pageviews, total, period, loading } = this.state;
    const totalUsers = users.length > 0 ? users.reduce(sumArray) : 0;
    const totalSessions = sessions.length > 0 ? sessions.reduce(sumArray) : 0;
    const totalPageviews =
      pageviews.length > 0 ? pageviews.reduce(sumArray) : 0;

    let avgVisitDurationInMinutes = total.avgSessionDurations
      ? Math.round(total.avgSessionDurations / 60)
      : 0;
    return (
      <div>
        <NextSeo
          title={"Analytics"}
          description={"Track your users, pageviews and visitors each Month."}
          noindex={true}
        />
        <div className="analyticsContainer">
          <h3 style={{ textAlign: "center", marginBottom: 20 }}>
            <strong>Analytics</strong>
          </h3>
          <div className="selectPeriodContainer">
            <label>Select a month</label>
            <MonthPicker
              placeholder="Select Month"
              onChange={this.handlePeriodChange}
              value={period}
            />
          </div>
          {loading ? (
            <div>
              <SectionLoader height={"500px"} />
            </div>
          ) : (
            <div>
              <div>
                <h4>{moment(period).format("MMMM YYYY")} Breakdown</h4>
                <div
                  style={{
                    display: "flex",
                    justifyContent: "space-between",
                    flexWrap: "wrap"
                  }}
                >
                  <Statistic title="Users" value={totalUsers} />
                  <Statistic title="Visits" value={totalSessions} />
                  <Statistic title="Pageviews" value={totalPageviews} />
                </div>
              </div>

              <div>
                <Tabs tabPosition="left">
                  <TabPane tab="Users" key="1">
                    {this.getStatisticsChart(users, "users", "dodgerblue")}
                  </TabPane>
                  <TabPane tab="Visits" key="2">
                    {this.getStatisticsChart(sessions, "sessions", "red")}
                  </TabPane>
                  <TabPane tab="Views" key="3">
                    {this.getStatisticsChart(pageviews, "pageviews", "green")}
                  </TabPane>
                </Tabs>

                <div style={{ marginTop: 40, fontSize: 16 }}>
                  <h4>
                    <strong>Analytics coming soon:</strong>
                  </h4>
                  <ul>
                    <li>Individual post analytics</li>
                    <li>Traffic sources</li>
                    <li>Top pages</li>
                  </ul>
                </div>
              </div>
            </div>
          )}
        </div>
        <style jsx>{`
          .analyticsContainer {
            margin: 20px auto;
            max-width: 700px;
          }
          .selectPeriodContainer {
            margin: 40px 0;
          }
        `}</style>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.payload.user
  };
};

export default withAuth(connect(mapStateToProps)(Analytics), true, true);
