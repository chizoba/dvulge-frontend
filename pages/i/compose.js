import React from "react";
import fetch from "isomorphic-unfetch";
import { withAuth, Head, ImageInput, Editor } from "../../components";
import { POST_CATEGORIES } from "../../utils/constants";
import { SET_PAGE_LOADING } from "../../store/constants";
import {
  Icon,
  Button,
  Upload,
  Image,
  Dropdown,
  Menu,
  Input,
  Tooltip,
  message,
  Modal,
  Drawer,
  Select
} from "antd";

import { Router } from "../../routes";
import { NextSeo } from "next-seo";
import api from "../../api";
import moment from "moment";

import { dataURLToBlob } from "blob-util";
import validDataUrl from "valid-data-url";
import { convertToHTML } from "draft-convert";

const { Option } = Select;
const { TextArea } = Input;

class Composer extends React.Component {
  //checks and returns post slug from query. if no slug it creates a new post and returns the slug
  static async getInitialProps({ req, res, query, store }) {
    store.dispatch({ type: "SET_ACTIVE_NAV", payload: "compose" });
    let { slug } = query;
    if (!slug) {
      let cookie = req ? req.headers.cookie : "";
      let response = await api.post.create({}, cookie);
      //handle api error
      if (response.error !== null) {
        store.dispatch({ type: SET_PAGE_LOADING, payload: false });
        return { statusCode: response.status };
      }
      //handle success
      let slug = response.data.post.slug;
      if (req) return res.redirect(`/i/compose/${slug}`);
      Router.replaceRoute("compose", { slug });
    }
    store.dispatch({ type: SET_PAGE_LOADING, payload: false });
    return { postSlug: slug };
  }

  constructor(props) {
    super(props);
    this.editor = React.createRef();
    this.state = {
      form: {
        description: "",
        tags: [],
        thumbnail: "",
        bodyHTML: "",
        categories: []
      },
      isPublished: false,
      publishing: false, //shows loading icon when publish button is clicked
      editorKey: Date.now(), //test
      errors: {},
      slug: this.props.postSlug, //slug can be updated
      lastUpdate: "",
      loading: false,
      showModal: false,
      blog: null
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleContextMenuClick = this.handleContextMenuClick.bind(this);
    this.saveForm = this.saveForm.bind(this);
    this.handlePublish = this.handlePublish.bind(this);
  }

  //initializes state
  async componentDidMount() {
    let slug = this.props.postSlug;
    let response = await api.post.get(slug);
    //handle api error
    if (response.error !== null) {
      return { statusCode: response.status };
    }
    this.setState({ editorKey: Date.now() });
    let post = response.data.post;
    this.setState({
      ...this.state,
      editorKey: Date.now(),
      form: {
        ...this.state.form,
        description: post.description || "",
        thumbnail: post.thumbnail || "",
        tags: post.tags,
        categories: post.categories
      },
      title: post.title,
      bodyHTML: post.bodyHTML || "",
      lastUpdate: post.dateUpdated,
      tags: post.tags,

      isPublished: post.isPublished
    });

    //fetch blog to get categories
    api.blog.get(post.blog.slug).then(response => {
      if (response.error !== null) return;
      const { blog } = response.data;

      this.setState({
        blog
      });
    });
  }

  async componentDidUpdate(prevProps, prevState) {
    if (prevProps.postSlug !== this.props.postSlug) {
      //do something
      window.location.reload(false); //force reload
    }
  }

  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;
    this.setState({
      ...this.state,
      form: { ...this.state.form, [name]: value }
    });
  }

  onDone = thumbnail => {
    this.setState({ form: { ...this.state.form, thumbnail } });
  };

  onDelete = () => {
    this.setState({ form: { ...this.state.form, thumbnail: "" } });
  };

  //not in use
  handleContextMenuClick(key) {
    if (key === "save") this.saveForm();
    else if (key === "advanced") this.setState({ showModal: true });
  }

  async handlePublish() {
    this.setState({ publishing: true });
    let { isPublished, errors } = this.state;
    await this.saveForm(isPublished);
    if (!isPublished && Object.keys(errors).length === 0)
      await this.publishPost();
    this.setState({ publishing: false });
  }

  async saveForm(alertOnSuccess = true) {
    //create form data
    this.setState({ loading: true, errors: {} });
    let data = this.state.form;
    let { title, content } = this.editor.current.getValues();
    let formData = new FormData();
    formData.append("title", title);
    formData.append("bodyHTML", content);
    for (let field in data) {
      let value;
      if (field === "thumbnail" && validDataUrl(data[field])) {
        value = dataURLToBlob(data[field]);
      } else if (field === "categories" || field === "tags") {
        value = data[field].toString();
      } else if (field === "bodyHTML") {
        continue;
      } else {
        value = data[field];
      }
      formData.append(field, value);
    }

    let response = await api.post.update(this.state.slug, formData);
    this.setState({ loading: false });
    if (response.error !== null) {
      let msg;
      if (response.status === 400) {
        let errors = response.error;
        let errorMsgs = Object.keys(errors).map((error, index) => {
          return (
            <span className={"formError"} key={index}>
              {errors[error].msg}
            </span>
          );
        });
        msg = (
          <div>
            <span>Correct Errors in form</span>
            {errorMsgs}
          </div>
        );
        this.setState({ errors: response.error });
      } else {
        msg = "Something went wrong. Please try again.";
      }
      return message.error(msg, 5);
    }
    this.setState({ dateUpdated: Date.now() });
    let { slug, thumbnail } = response.data.post;
    if (slug !== this.state.slug) {
      //update slug if changed
      this.setState({ slug });
      Router.replaceRoute("compose", { slug }, { shallow: true });
    }
    if (thumbnail && thumbnail !== this.state.form.thumbnail) {
      this.setState({ form: { ...this.state.form, thumbnail } });
    }
    if (alertOnSuccess) message.success("Saved!");
    return;
  }

  //publish post
  async publishPost() {
    if (!this.state.form.thumbnail) {
      return this.setState({
        errors: {
          ...this.state.errors,
          thumbnail: "Please provide a thumbnail"
        }
      });
    }
    let response = await api.post.publish(this.state.slug); //publish post
    //if error or no post found display error message
    if (response.error) {
      let { status } = response;
      if (
        status === 403 ||
        status === 400 ||
        status === 401 ||
        status === 404
      ) {
        return message.error(response.error.message);
      }
      return message.error(
        "This post could not be published. Try again later or Refresh this page"
      );
    }
    let msg = this.state.isPublished
      ? "Updated"
      : "Your post has been published";
    this.setState({ isPublished: true, showModal: false });
    return message.success(msg);
  }

  render() {
    let { description, tags, thumbnail, categories } = this.state.form;
    let {
      errors,
      title,
      bodyHTML,
      loading,
      showModal,
      blog,
      publishing,
      isPublished
    } = this.state;
    const tagOptions = [];
    for (let i = 10; i < 36; i++) {
      tagOptions.push(
        <Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>
      );
    }
    let okText = isPublished ? "Update" : "Publish";
    return (
      <div>
        <NextSeo
          title={"Compose"}
          description={"Create a new post on Dvulge."}
          noindex={true}
        />

        <div className="composerContainer">
          <form>
            <Modal
              title="Publish Settings"
              visible={showModal}
              okText={okText}
              confirmLoading={publishing}
              cancelText="Close"
              onOk={this.handlePublish}
              onCancel={() => {
                this.setState({ showModal: false });
              }}
            >
              <div className="formItem">
                <label>Thumbnail (Required)</label>
                <span className={"formError"}>{errors.thumbnail}</span>
                <ImageInput
                  image={thumbnail}
                  onDone={this.onDone.bind(this)}
                  width={260}
                  height={175}
                  onDelete={this.onDelete.bind(this)}
                  onError={error =>
                    this.setState({ errors: { ...errors, thumbnail: error } })
                  }
                />
              </div>
              <div className="formItem">
                <label>Description</label>
                <span className={"formError"}>{errors.description}</span>
                <TextArea
                  style={{
                    maxWidth: "100%",
                    resize: "none"
                  }}
                  name="description"
                  placeholder="description"
                  onChange={this.handleChange}
                  value={description}
                  autosize
                />
              </div>
              <div className="formItem">
                <label>Add to Category</label>
                <span className={"formError"}>{errors.tags}</span>
                <Select
                  style={{ width: "100%" }}
                  mode="multiple"
                  placeholder="Select a category"
                  value={categories}
                  onChange={categories =>
                    this.setState({ form: { ...this.state.form, categories } })
                  }
                >
                  {blog && blog.categories.length > 0 ? (
                    blog.categories.map((category, index) => {
                      return (
                        <Option key={index} value={category}>
                          {category}
                        </Option>
                      );
                    })
                  ) : (
                    <Option value="none" disabled>
                      None
                    </Option>
                  )}
                </Select>
              </div>
              <div className="formItem">
                <label>
                  <Icon type="tag" /> Add tags. Make your post discoverable
                  <span className={"formError"}>{errors.tags}</span>
                </label>

                <Select
                  mode="multiple"
                  style={{ width: "100%" }}
                  value={tags}
                  onChange={tags =>
                    this.setState({ form: { ...this.state.form, tags } })
                  }
                  placeholder="Enter Tags"
                >
                  {POST_CATEGORIES.map((category, index) => {
                    return (
                      <Option key={index} value={category.toLowerCase()}>
                        {category}
                      </Option>
                    );
                  })}
                </Select>
              </div>
            </Modal>

            <Editor
              ref={this.editor}
              key={this.state.editorKey}
              title={title}
              content={bodyHTML}
              onSave={this.saveForm}
              onPublish={() => this.setState({ showModal: true })}
              contextMenu={[
                { key: "save", value: "Save Draft" },
                { key: "advanced", value: "Edit thumbnail, tags & more" }
              ]}
              onContextMenuClick={this.handleContextMenuClick}
            />
          </form>
        </div>

        <style jsx>
          {`
            label {
              display: block;
            }
            .previewContainer {
              border: 1px dashed #ccc;
              height: 150px;
              width: 300px;
              background: #f9f9f9;
              text-align: center;
              margin: 10px 0;
            }
            form {
              // max-width: 720px;
              max-width: 900px;
              margin: auto;
              padding: 10px;
              background: white;
            }
          `}
        </style>
      </div>
    );
  }
}
export default withAuth(Composer, true, true);
