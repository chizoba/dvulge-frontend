import "antd/dist/antd.css";
import {
  Tabs,
  Icon,
  Input,
  Select,
  Spin,
  Statistic,
  Button,
  Modal,
  message,
  Empty,
  Collapse
} from "antd";
import { withAuth, Head, Card, FlatPostCard } from "../../components";
const { Option } = Select;
const { TabPane } = Tabs;
const { Search } = Input;
const { confirm } = Modal;
import moment from "moment";
import api from "../../api";
// import Link from "next/link";
import { Router, Link } from "../../routes";
import React from "react";
import { connect } from "react-redux";
import { useEffect, useState } from "react";
import { NextSeo } from "next-seo";
import { SET_PAGE_LOADING } from "../../store/constants";

function Posts(props) {
  //props
  const { page, status, user } = props;

  //local states
  let [posts, setPosts] = useState([]);
  const [search, setSearch] = useState(props.search); //we need to control the search input box
  let [morePostsAvailable, setMorePostsAvailable] = useState(false);
  let [fetchError, setFetchError] = useState(false);
  let [loading, setLoading] = useState(false);

  //on component mounted
  useEffect(() => {
    loadPosts();
  }, []);

  //on prop changes to page, status or search reload post list
  useEffect(() => {
    loadPosts();
    if (props.search !== search) setSearch(props.search);
  }, [props.page, props.search, props.status]);

  const pushRoute = query => {
    //injects new route that updates props
    if (page && !query.hasOwnProperty("page")) query.page = page;
    if (search && !query.hasOwnProperty("search")) query.search = search;
    if (status && !query.hasOwnProperty("status")) query.status = status;
    Router.pushRoute("myposts", query);
  };

  //load posts based on props
  const loadPosts = async () => {
    setLoading(true); //show loading icon
    const blog = user.blog;
    let published = status ? status.toLowerCase() === "published" : "";
    let response = await api.post.list({
      blog: blog.slug,
      page,
      search: props.search, //always use the props value. state value may not be updated immediately after the browser histroy button is used.
      published
    });
    //handle error
    if (response.error !== null) {
      setFetchError(true);
      setPosts([]);
      setMorePostsAvailable(false);
      setLoading(false);
      return;
    }
    //on success
    let { posts, numOfPages } = response.data;
    let morePostsAvailable = page < numOfPages;

    setPosts(posts);
    setMorePostsAvailable(morePostsAvailable);
    setLoading(false);
  };

  //publish post: deprecated
  const publishPost = async post => {
    if (!props.user.isVerified) {
      return message.error("Please verify your email or login again.");
    }
    if (!post.thumbnail) {
      return Modal.info({
        title: "Thumbnail Required",
        content: (
          <div>
            <p>All published posts on Dvulge require a thumbnail.</p>
            <p>Here is how to add one. </p>
            <ul>
              <li>Click Edit.</li>
              <li>
                Click the <Icon type="down" /> button on the Editor toolbar.
              </li>
              <li>Click Advance.</li>
            </ul>
          </div>
        ),
        onOk() {}
      });
    }
    const { slug } = post;
    confirm({
      title: "Do you want to publish this Post",
      content: `This post will become visible on your blog `,
      onOk: async () => {
        let response = await api.post.publish(slug); //publish post
        //if error or no post found display error message
        if (response.error) {
          let { status } = response;
          if (
            status === 403 ||
            status === 400 ||
            status === 401 ||
            status === 404
          ) {
            return message.error(response.error.message);
          }
          message.error(
            "This post could not be published. Try again later or Refresh this page"
          );
          //ToDo report error to server
          return;
        }
        //if no errors update posts and return success message

        const updatedPostList = posts.map(post => {
          if (post.slug === slug) return { ...post, isPublished: true };
          return post;
        });
        setPosts(updatedPostList);
        message.success("Your post has been published");
      },
      onCancel() {} //do nothing
    });
  };
  //delete post
  const deletePost = async slug => {
    if (!user.isVerified) {
      return message.error("Please verify your email first or login again.");
    }
    confirm({
      title: "Do you want to delete this Post",
      content: `This post will not be visible to the public
            and will be removed from the system within 24 hours `,
      onOk: async () => {
        let response = await api.post.delete(slug); //delete post
        //if error or no post found display error message
        if (response.error) {
          let { status } = response;
          if (status === 403 || status === 401 || status === 404) {
            return message.error(response.error.message);
          }
          message.error(
            "This post could not be deleted. Try again later or Refresh this page"
          );
          //ToDo report error to server
          return;
        }
        //reload page aftet delete
        let loadPage = page;
        if (posts.length === 1 && page > 1) loadPage = loadPage - 1;
        message.success("Post has been deleted");
        return loadPosts(); //reload page
      },
      onCancel() {}
    });
  };

  const postCards = posts.map((post, index) => {
    return (
      <FlatPostCard
        key={index}
        post={post}
        onPublish={publishPost}
        onDelete={deletePost}
      />
    );
  });

  return (
    <div>
      <NextSeo
        title={"My Posts"}
        description={"Create and Manage your posts and drafts on Dvulge."}
        noindex={true}
      />
      <div className="myPostsContainer">
        <h3 style={{ textAlign: "center" }}>
          <strong>My Posts</strong>
        </h3>
        <Button
          shape="round"
          ghost
          type="primary"
          style={{ marginTop: 40 }}
          onClick={() => Router.pushRoute("compose")}
        >
          Create a Post <Icon type="edit" />
        </Button>
        <div style={{ margin: "40px 0" }}>
          <div>
            <Search
              placeholder="Search "
              value={search}
              onChange={e => {
                setSearch(e.target.value);
              }}
              onSearch={value => {
                pushRoute({ search: value, page: 1 });
              }}
              style={{
                width: 150,
                marginRight: "10px",
                marginBottom: "10px"
              }}
            />

            <Select
              value={status}
              style={{ width: 120 }}
              onChange={value => {
                pushRoute({ status: value, page: 1 });
              }}
            >
              <Option value="">Show All</Option>
              <Option value="published">Published</Option>
              <Option value="drafts">Drafts</Option>
            </Select>
          </div>
          {postCards}
        </div>
        <div className="pagination">
          <span>
            {page > 1 && ( //if page is greater than 1 show previous button
              <a
                className="link"
                onClick={() => {
                  pushRoute({ page: page - 1 });
                }}
              >
                <Icon type="arrow-left" /> Previous
              </a>
            )}
          </span>
          <span>
            {morePostsAvailable && (
              <a
                className="link"
                onClick={() => {
                  pushRoute({ page: page + 1 });
                }}
              >
                Next <Icon type="arrow-right" />
              </a>
            )}
          </span>
        </div>
      </div>
      <style>
        {`
              .myPostsContainer {
                  margin: 20px auto;
                  max-width: 700px;
              }

              .logoContainer {
                  width: 200px;
                  max-height:200px;
              }
              .logoContainer img {
                  max-width:100%;
              }
              .tabsContainer {
                  margin: 30px 0;
                  background: white;
                  min-height: 500px;
                  padding: 5px 10px ;
              }
              .pagination {
                display:flex;
                font-size:16px;
                width: 200px;
                margin: 10px auto;
                justify-content:space-between
              }
              .loadingPost{
                height:400px;
                width: 100%;
                display:flex;
                justify-content:center;
                align-items:center;
              }


              `}
      </style>
    </div>
  );
}

Posts.getInitialProps = ({ query, store }) => {
  store.dispatch({ type: "SET_ACTIVE_NAV", payload: "my-posts" });
  let { page, status, search } = query;
  page = page ? Number(page) : 1;
  status = status ? status : "";
  search = search ? search : "";
  store.dispatch({ type: SET_PAGE_LOADING, payload: false });
  return { page, status, search };
};

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.payload.isAuthenticated,
    user: state.auth.payload.user
  };
};

export default withAuth(connect(mapStateToProps)(Posts), true, true);
