//Configure blog and user profile
import { Head, BlogForm, withAuth, ProfileForm } from "../../components";
import { Tabs, Icon, Input } from "antd";
const { TabPane } = Tabs;
import { NextSeo } from "next-seo";
import { SET_PAGE_LOADING } from "../../store/constants";
function Settings({ user }) {
  return (
    <div>
      <NextSeo
        title={"Settings"}
        description={"Edit your Dvulge blog and profile."}
        noindex={true}
      />

      <div className="settingsContainer">
        <h3 style={{ textAlign: "center" }}>
          <strong> Settings</strong>
        </h3>
        <div className="settingsTabsContainer">
          <Tabs defaultActiveKey="user">
            <TabPane
              tab={
                <span>
                  <Icon type="user" />
                  Profile
                </span>
              }
              key="user"
            >
              <ProfileForm />
            </TabPane>
            <TabPane
              tab={
                <span>
                  <Icon type="layout" />
                  Blog
                </span>
              }
              key="blog"
            >
              <BlogForm editMode={true} />
            </TabPane>
          </Tabs>
        </div>
      </div>
      <style>
        {`
            .settingsContainer {
              margin: 20px auto;
              max-width: 700px;
            }
            .settingsTabsContainer{
              margin:40px 0;
            }

            `}
      </style>
    </div>
  );
}

Settings.getInitialProps = ({ store }) => {
  store.dispatch({ type: "SET_ACTIVE_NAV", payload: "settings" });
  store.dispatch({ type: SET_PAGE_LOADING, payload: false });
};
export default withAuth(Settings, true, true);
