import React, { useState } from "react";
import Head from "../components/Head";
import { Icon, Card } from "antd";
import { withAuth, RegistrationForm } from "../components";
import { BLOG_FEATURES } from "../utils/constants";
import { SET_PAGE_LOADING } from "../store/constants";

const { Meta } = Card;

const Index = () => {
  const featuresListCard = BLOG_FEATURES.map(feature => {
    return (
      <Card
        key={feature.id}
        hoverable
        style={{
          width: 300,
          marginRight: 10,
          marginBottom: 10,
          display: "inline-block"
        }}
        cover={<img alt={feature.alt} src={feature.image} height="200px" />}
      >
        <Meta title={feature.title} description={feature.description} />{" "}
      </Card>
    );
  });

  return (
    <div>
      <Head
        title="Dvulge"
        description={`Create a blog for free and focus on what matters (content and audience).
            Link your social media account and get instant analytics with zero configuration.`}
        allowIndex={true}
      />
      <div className="indexContainer">
        <div className="indexSection">
          <h2>
            <strong>
              Join your fellow bloggers. <br /> today
            </strong>
          </h2>
          <div
            style={{
              maxWidth: 400,
              textAlign: "center",
              margin: "20px auto"
            }}
          >
            <img
              src="https://cdn.dvulge.com/static/group.png"
              style={{
                maxWidth: "100%"
              }}
            />
          </div>
          <RegistrationForm />
        </div>
        <div
          className="indexSection"
          style={{
            backgroundColor: "#f5f5f5"
          }}
        >
          <div className="blogPreview">
            <h3>
              <strong>
                Simple setup. <br /> No plugins
              </strong>
            </h3>
            <div>
              <img
                src="https://cdn.dvulge.com/static/dvulge-what-if.png"
                style={{
                  maxWidth: "100%"
                }}
              />
            </div>
          </div>
        </div>
        <div className="indexSection">
          <h4>
            <strong> Comes with </strong>
          </h4>
          <div className="indexFeatures"> {featuresListCard} </div>
        </div>
        <div className="indexSection indexInfoSection">
          <h5>
            <strong> in case you are curious </strong>
          </h5>
          <p> Yes, Dvulge is free for everyone. </p>
          <p>
            Creating an account is similar to creating a social media account.
            You do not select a layout/template.
          </p>
          <p>
            Currently, you can not earn money on Dvulge. However, this feature
            is arriving soon.
          </p>
        </div>
      </div>
      <style>
        {`
              .indexContainer {
                margin:20px 0;
              }
              .indexSection {
                padding: 20px;
                margin-top:40px;
                text-align:center;
              }
              .blogPreview {
                max-width:800px;
                max-height:100%;
                margin:20px auto;
              }
              .indexFeatures {
                  margin:0 auto;
              }
              .indexInfoSection{
                  font-size:16px;
              }
              .signUpFormButton{
                display: block;
                margin:auto;
              }
              @media only screen and (min-width:600px){
                  .signUpFormButton{
                      display: inline-block;
                  }
              }
            `}
      </style>
    </div>
  );
};

Index.getInitialProps = ({ store }) => {
  store.dispatch({ type: SET_PAGE_LOADING, payload: false });
};

export default withAuth(Index);
