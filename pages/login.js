import { useState, useEffect } from "react";
import { connect } from "react-redux";
import { PageLoader, Head } from "../components";
import { Icon, Alert } from "antd";
import { Router } from "../routes";

import { login } from "../store/actions";
import { SERVER_ERROR_MSG } from "../utils/constants";
import { SET_PAGE_LOADING } from "../store/constants";
function Login(props) {
  const { dispatchLogin, token, user, auth } = props;
  let [loading, setLoading] = useState(true); //initial loading
  let [message, setMessage] = useState(); //initial loading

  //on component mounted dispatch login request
  useEffect(() => {
    const validateLogin = async () => {
      //dispatch login action
      dispatchLogin(token);
    };
    validateLogin();
  }, []);

  //updates local state based on auth .
  useEffect(() => {
    function checkAuth() {
      if (auth.error === null) {
        if (Object.keys(user).length > 0) {
          const { blog } = user;
          if (blog && blog.slug) {
            return Router.pushRoute("blog", { slug: blog.slug });
          }
          return Router.pushRoute("createBlog");
        }
      } else {
        if (auth.status === 400) {
          setMessage({
            message: "Invalid/Expired token",
            description: "Login to get a new token.",
            type: "error"
          });
        } else {
          setMessage({
            message: "500",
            description: SERVER_ERROR_MSG,
            type: "error"
          });
        }
        setLoading(false);
      }
    }
    checkAuth();
  }, [auth]);
  return (
    <div>
      {/*<Head title={"Login"} allowIndex={false} /> this produces an error*/}
      <div className="loginContainer">
        {loading ? (
          <PageLoader text="Validating" />
        ) : (
          <div>
            <div>
              <Alert
                message={message.message}
                description={message.description}
                type={message.type}
                showIcon
              />
            </div>
          </div>
        )}
      </div>
      <style jsx>
        {`
          .loginContainer {
            max-width: 500px;
            padding: 50px;
            margin: auto;
          }
        `}
      </style>
    </div>
  );
}

//return token as prop
Login.getInitialProps = ({ query, store }) => {
  store.dispatch({ type: SET_PAGE_LOADING, payload: false });
  return { token: query.token };
};

const mapStateToProps = state => {
  return {
    user: state.auth.payload.user,
    auth: state.auth
  };
};
const mapDispatchToProps = dispatch => {
  return {
    dispatchLogin: token => dispatch(login(token))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
