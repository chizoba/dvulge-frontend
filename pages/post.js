import React, { Fragment } from "react";
import { Head, withAuth, PostImage } from "../components";
import { Icon, Card, Dropdown, Menu } from "antd";
import { shareLinkMenu, getPostURL } from "../utils";
import moment from "moment";
import { Link } from "../routes";
import { useState, useEffect } from "react";
import api from "../api";
import ReactHtmlParser, {
  processNodes,
  convertNodeToElement,
  htmlparser2
} from "react-html-parser";
import { NextSeo } from "next-seo";
import { SET_PAGE_LOADING } from "../store/constants";

function transform(node) {
  console.log(node);
  if (
    node.type === "tag" &&
    (node.name === "p" || node.name === "h3") &&
    node.children.length === 0
  ) {
    return <br />;
  }
  if (node.type === "tag" && node.name === "img") {
    const { src, className, title, width } = node.attribs;
    return (
      <PostImage
        src={src}
        className={node.attribs.class}
        width={width}
        caption={title}
      />
    );
  }
}

function createMarkup(html) {
  return <div>{ReactHtmlParser(html, { transform })}</div>;
}

function Post(props) {
  let [blog, setBlog] = useState(props.blog);
  let [nextPost, setNextPost] = useState(null);
  let [previousPost, setPreviousPost] = useState(null);
  let [like, setLike] = useState(false);
  let [likeCount, setLikeCount] = useState(0);

  const postURL = getPostURL(blog.slug, props.slug);

  useEffect(() => {
    const getLike = async () => {
      //checks if this reader has liked this post or not
      let response = await api.like.get(props._id);
      if (response.error !== null) return; //silently fail
      setLikeCount(response.data.likes);
      setLike(response.data.likedByUser);
    };
    const recommendPosts = async () => {
      //gets next and previous post
      let response = await api.post.recommend(props.slug);
      //handle error
      if (response.error !== null) return; //fail silently
      let { nextPost, previousPost } = response.data;
      setPreviousPost(previousPost || "");
      setNextPost(nextPost || "");
    };
    getLike();
    recommendPosts();
  }, [props._id]);

  //displays publsihed, updated or created date based on post's status
  const getDisplayDate = () => {
    let displayDate = "";
    if (props.isPublished) {
      displayDate = `Posted ${moment(props.datePublished).format("ll")}.`;
      if (
        props.dateUpdated &&
        !moment(props.datePublished).isSame(moment(props.dateUpdated), "day")
      ) {
        displayDate =
          displayDate + ` Updated ${moment(props.dateUpdated).format("ll")}`;
      }
    } else {
      displayDate = `Created ${moment(props.dateCreated).format("ll")}.`;
    }
    return displayDate;
  };

  const toggleLike = async () => {
    let id = props._id;

    if (like) {
      let response = await api.like.delete(id);
      if (response.error !== null) return; //silently fail

      setLike(false);
      setLikeCount(likeCount - 1);
    } else {
      let response = await api.like.create(id);
      if (response.error !== null) return; //silently fail
      setLike(true);
      setLikeCount(likeCount + 1);
    }
  };

  return (
    <div>
      <NextSeo
        title={props.title}
        description={props.description}
        allowIndex={true}
        openGraph={{
          url: getPostURL(blog.slug, props.slug),
          title: props.title,
          type: "article",
          description: props.description,
          images: [{ url: props.thumbnail }],
          article: {
            publishedTime: props.datePublished,
            modifiedTime: props.dataUpdated
          }
        }}
      />

      <div className="postContainer">
        <section
          style={{
            maxWidth: 700,
            minWidth: 300,
            flex: 1,
            padding: 10,
            margin: "auto"
          }}
        >
          <Link route="blog" params={{ slug: blog.slug }}>
            <a>
              <strong className="link">{blog.title}</strong>
            </a>
          </Link>
          <p style={{ fontSize: 14, color: "#777" }}>{getDisplayDate()}</p>
          {!props.isPublished && (
            <p style={{ fontSize: 14 }}>
              <Icon type="info-circle" style={{ color: "dodgerblue" }} /> This
              article is still a draft and has not been published.
            </p>
          )}

          <div className="postHeader">
            <h1>{props.title}</h1>
            <p className="postDescription">{props.description}</p>
          </div>
          {props.thumbnail && (
            <div className="postThumbnail">
              <img src={props.thumbnail} />
            </div>
          )}
          <div className="postContent">
            {/*dangerouslySetInnerHTML={createMarkup(props.bodyHTML)}*/}
            <div className="postBody" />
            {createMarkup(props.bodyHTML)}
          </div>
          <div
            className="postInteractions"
            style={{ textAlign: "center", marginTop: 20 }}
          >
            <span
              onClick={toggleLike}
              style={{ marginRight: 15 }}
              className="likeContainer"
            >
              <Icon type="heart" style={{ color: like ? "red" : "" }} /> Like
              {likeCount >= 1 && ` (${likeCount})`}
            </span>
            <Dropdown
              overlay={shareLinkMenu(postURL)}
              placement="bottomLeft"
              trigger={["click", "hover"]}
            >
              <span>
                {" "}
                Share <Icon type="down" />
              </span>
            </Dropdown>
          </div>
          <div className="otherPosts">
            <div className="previousPost">
              {previousPost && (
                <Fragment>
                  <p>
                    <Icon type="arrow-left" /> Previous
                  </p>
                  <div>
                    <Link
                      route="post"
                      params={{
                        postSlug: previousPost.slug,
                        blogSlug: blog.slug
                      }}
                    >
                      <a className="link">{previousPost.title}</a>
                    </Link>
                  </div>
                </Fragment>
              )}
            </div>

            <div className="nextPost">
              {nextPost && (
                <Fragment>
                  <p>
                    Next <Icon type="arrow-right" />
                  </p>
                  <div>
                    <Link
                      route="post"
                      params={{
                        postSlug: nextPost.slug,
                        blogSlug: blog.slug
                      }}
                    >
                      <a className="link">{nextPost.title}</a>
                    </Link>
                  </div>
                </Fragment>
              )}
            </div>
          </div>
        </section>

        <div className="adsContainer">
          <p>Ads</p>
        </div>
      </div>

      <style jsx>
        {`
          .postContainer {
            padding: 10px 10px;
            padding-top: 20px;
            margin: auto;
            margin-bottom: 50px;
            color: #4c4e4d !important;
            display: flex;
            flex-wrap: wrap;
          }

          .postHeader {
            margin: 20px auto;
            margin-bottom: 40px;
          }
          .postContent {
            line-height: 2em;
            text-align: justify;
            -webkit-hyphens: auto;
            -moz-hyphens: auto;
            hyphens: auto;
          }
          //.postContent p added in layout component to remove antd css

          .postThumbnail {
            width: 100%;
            margin-bottom: 40px;
          }
          .postThumbnail > img {
            width: 100%;
          }
          .postDescription {
            color: #999;
            font-size: 16px;
          }
          .postInteractions {
            margin-bottom: 40px;
          }
          .likeContainer :hover {
            cursor: pointer;
          }
          .otherPosts {
            display: flex;
          }
          .previousPost,
          .nextPost {
            flex: 1;
            margin: 5px;
            padding: 5px;
          }
          .previousPost {
            text-align: right;
          }
          .adsContainer {
            width: 310px;
            padding: 5px;
          }
        `}
      </style>
    </div>
  );
}

Post.getInitialProps = async ({ req, query, store }) => {
  store.dispatch({ type: SET_PAGE_LOADING, payload: true });
  const res = await api.post.get(query.postSlug);
  store.dispatch({ type: "SET_ACTIVE_NAV", payload: "" });
  if (res.error !== null) return { statusCode: res.status };
  let data = await res.data;
  let props = {
    _id: data.post._id,
    title: data.post.title,
    slug: data.post.slug,
    body: data.post.body,
    bodyHTML: data.post.bodyHTML,
    datePublished: data.post.datePublished,
    dateUpdated: data.post.dateUpdated,
    dateCreated: data.post.dateCreated,
    isPublished: data.post.isPublished,
    blog: data.post.blog,
    editor: data.post.editor,
    thumbnail: data.post.thumbnail,
    description: data.post.description
  };
  store.dispatch({ type: SET_PAGE_LOADING, payload: false });
  return props;
};

export default withAuth(Post);
