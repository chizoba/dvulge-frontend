const routes = require("next-routes");
const Router = routes();

//name, pattern,page
Router.add("index", "/") //index page
  .add("guide", "/guide") //settings page
  .add("blog", "/:slug", "blog") //Get blog
  .add("createBlog", "/i/create-blog") //create a blog
  .add("analytics", "/i/analytics", "i/analytics") //analytics page
  .add("myposts", "/i/posts", "i/posts") //analytics page
  .add("settings", "/i/settings", "i/settings") //settings page
  .add("compose", "/i/compose/:slug?", "i/compose") //compose post page
  .add("login", "/i/login/:token", "login") //settings page
  .add("post", "/:blogSlug/post/:postSlug", "post"); //Get post

module.exports = Router;
