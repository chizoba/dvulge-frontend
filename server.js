const express = require("express");
const next = require("next");
const Router = require("./routes");
const app = next({
  dev: process.env.NODE_ENV !== "production"
});

const handler = Router.getRequestHandler(app);
// With express
app.prepare().then(() => {
  const server = express();
  server.use(function(req, res, next) {
    if (
      req.get("x-forwarded-proto") !== "https" &&
      process.env.NODE_ENV === "production"
    ) {
      return res.redirect("https://" + req.get("Host") + req.url);
    } else next();
  });
  server.use(handler).listen(process.env.PORT || 5000);
});
