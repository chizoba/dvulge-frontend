import {
  AUTH_LOGIN_FAILURE,
  AUTH_LOGIN_REQUEST,
  AUTH_LOGIN_SUCCESS,
  UPDATE_AUTH_USER_FAILURE,
  UPDATE_AUTH_USER_REQUEST,
  UPDATE_AUTH_USER_SUCCESS,
  AUTH_LOGOUT_FAILURE,
  AUTH_LOGOUT_REQUEST,
  AUTH_LOGOUT_SUCCESS
} from "../constants";
import api from "../../api";

export function logout() {
  return async dispatch => {
    dispatch({ type: AUTH_LOGOUT_REQUEST });
    try {
      const result = await api.session.delete();
      if (result.error !== null)
        throw { error: result.error, status: result.status };
      //if no errors add user to store
      return dispatch({ type: AUTH_LOGOUT_SUCCESS });
    } catch (err) {
      return dispatch({
        type: AUTH_LOGOUT_FAILURE,
        error: err.error,
        status: err.status
      });
    }
  };
}

export function login(token) {
  return async dispatch => {
    dispatch({ type: AUTH_LOGIN_REQUEST });
    try {
      const result = await api.session.create({ token });
      if (result.error) throw { error: result.error, status: result.status };
      const user = result.data.user;
      dispatch({ type: AUTH_LOGIN_SUCCESS, payload: user });
    } catch (err) {
      dispatch({
        type: UPDATE_AUTH_USER_FAILURE,
        error: err.error,
        status: err.status
      });
    }
  };
}
export function updateAuthUser(data) {
  return async dispatch => {
    dispatch({ type: UPDATE_AUTH_USER_REQUEST });
    try {
      const result = await api.user.update(data);
      if (result.error) throw { error: result.error, status: result.status };
      const user = result.data.user;
      dispatch({ type: UPDATE_AUTH_USER_SUCCESS, payload: user });
    } catch (err) {
      dispatch({
        type: UPDATE_AUTH_USER_FAILURE,
        error: err.error,
        status: err.status
      });
    }
  };
}

export function deleteAuthUser() {
  return async dispatch => {
    dispatch({ type: DELETE_AUTH_USER_REQUEST });
    try {
      const result = await api.user.delete();
      if (result.error) throw { error: result.error, status: result.status };
      const user = result.data.user;
      dispatch({ type: DELETE_AUTH_USER_SUCCESS, payload: user });
    } catch (err) {
      dispatch({
        type: DELETE_AUTH_USER_FAILURE,
        error: err.error,
        status: err.status
      });
    }
  };
}
