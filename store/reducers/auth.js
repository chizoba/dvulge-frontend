import {
  AUTH_LOGIN_FAILURE,
  AUTH_LOGIN_REQUEST,
  AUTH_LOGIN_SUCCESS,
  UPDATE_AUTH_USER_FAILURE,
  UPDATE_AUTH_USER_REQUEST,
  UPDATE_AUTH_USER_SUCCESS,
  AUTH_LOGOUT_FAILURE,
  AUTH_LOGOUT_REQUEST,
  AUTH_LOGOUT_SUCCESS
} from "../constants";

const initialState = {
  payload: { user: {}, isAuthenticated: false },
  error: null,
  status: null,
  isLoading: false
};

export default function auth(state = initialState, action) {
  switch (action.type) {
    case AUTH_LOGIN_REQUEST:
      return {
        ...state,
        isLoading: true
      };
    case AUTH_LOGIN_SUCCESS:
      return {
        ...state,
        payload: {
          ...state.payload,
          user: action.payload,
          isAuthenticated: true
        },
        isLoading: false,
        error: null,
        status: null
      };
    case AUTH_LOGIN_FAILURE:
      return {
        ...state,
        payload: {
          ...state.payload,
          isAuthenticated: false
        },
        error: action.error,
        status: action.status,
        isLoading: false
      };
    case AUTH_LOGOUT_REQUEST:
      return {
        ...state,
        isLoading: true
      };
    case AUTH_LOGOUT_SUCCESS:
      return {
        ...state,
        payload: {
          ...state.payload,
          user: {},
          isAuthenticated: false
        },
        isLoading: false,
        error: null,
        status: null
      };
    case AUTH_LOGOUT_FAILURE:
      return {
        ...state,
        error: action.error,
        status: action.status,
        isLoading: false
      };
    case UPDATE_AUTH_USER_REQUEST:
      return {
        ...state,
        isLoading: true
      };
    case UPDATE_AUTH_USER_SUCCESS:
      return {
        ...state,
        payload: {
          ...state.payload,
          user: action.payload,
          isAuthenticated: true
        },
        isLoading: false,
        error: null,
        status: null
      };
    case UPDATE_AUTH_USER_FAILURE:
      return {
        ...state,
        error: action.error,
        status: action.status,
        isLoading: false
      };

    default:
      return state;
  }
}
