import { combineReducers } from "redux";
import auth from "./auth";
import nav from "./nav";
import { SET_PAGE_LOADING } from "../constants";

const initialState = { pageLoading: false };

function main(state = initialState, action) {
  switch (action.type) {
    case SET_PAGE_LOADING:
      return {
        ...state,
        pageLoading: action.payload
      };
    default:
      return state;
  }
}

const rootReducer = combineReducers({
  main,
  auth,
  nav
});

export default rootReducer;
