import { SET_ACTIVE_NAV, TOGGLE_MENU } from "../constants";

const initialState = {
  active: "",
  showMenu: true
};

export default function nav(state = initialState, action) {
  switch (action.type) {
    case SET_ACTIVE_NAV:
      return {
        ...state,
        active: action.payload
      };
    case TOGGLE_MENU:
      return {
        ...state,
        showMenu: action.payload
      };
    default:
      return state;
  }
}
