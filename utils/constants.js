export const SERVER_ERROR_MSG = "Sorry, Something went wrong. Try again later.";
export const INVALID_REQUEST_MSG = "Invalid Request.";
export const LOGIN_SUCCESS_MSG =
  "Hi there, we sent a login link to your email. Please check your inbox.";
export const BLOG_FEATURES = [
  {
    id: 1,
    title: "Google Analytics",
    description: "Track your users, visitors and total pageviews every day.",
    image:
      "https://developers.google.com/analytics/images/terms/logo_lockup_analytics_icon_vertical_white_2x.png"
  },
  {
    id: 2,
    title: "Rich Text Editor",
    description: "Create interesting articles with our custom editor. ",
    image: "https://cdn.dvulge.com/static/dvulge-editor.png"
  }
  // {
  //   title: "Social Media Integration",
  //   description:
  //     "Gain fans by linking your social media profile to your blog or Embedding your media posts to your articles.",
  //   image: "https://etimg.etb2bimg.com/photo/69526336.cms"
  // }
];
export const POST_CATEGORIES = [
  "Adventure",
  "Africa",
  "Art",
  "Baby",
  "Beauty",
  "Business",
  "Books",
  "Cars",
  "Craft",
  "Career",
  "Cooking",
  "Decorating",
  "Design",
  "DIY",
  "Education",
  "Entertainment",
  "Europe",
  "Fashion",
  "Film",
  "Fitness",
  "Food",
  "Gaming",
  "Health",
  "Home",
  "Humor",
  "Kids",
  "Lifestyle",
  "Makeup",
  "Marketing",
  "Men’s",
  "Mom",
  "Money",
  "Movies",
  "Music",
  "Nature",
  "New",
  "Outdoor",
  "Parenting",
  "Personal",
  "Pets",
  "Photography",
  "Politics",
  "Relationships",
  "Self-help",
  "Sewing",
  "Sports",
  "Tech",
  "Travel",
  "Wedding",
  "Women's"
];
