//returns an editor's profile from the Id
import api from "../api";

const getEditor = async editorId => {
  const response = await api.user.get(editorId);
  if (response.error !== null) return null; //if error occurs silently fail
  return response.data.user;
};
export default getEditor;
