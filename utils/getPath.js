export function getBlogURL(blog) {
  return process.env.APP_URL + "/" + blog;
}

//recevies a blog and post slug and returns the url.
export function getPostURL(blog, post) {
  return process.env.APP_URL + "/" + blog + "/post/" + post;
}

//recevies a blog and post slug and returns the url.
export function getAppURL(blog, post) {
  return process.env.APP_URL;
}
