//recevies a blog and post slug and returns the url.
export default function getPostURL(blog, post) {
  const postURL = process.env.APP_URL + "/" + blog + "/post/" + post;
  return postURL;
}
