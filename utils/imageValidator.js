//takes in a File object and validates that it matches a size and image format
//size - an int specifying the mb
export default function imageValidator(image, size = 5) {
  const acceptedImageTypes = [
    "image/gif",
    "image/jpeg",
    "image/jpg",
    "image/png"
  ];
  //validate file type
  if (!acceptedImageTypes.includes(image.type)) {
    throw "Invalid image format. Image must be jpg, jpeg, png or GIF";
  }
  //validate size
  let imageSize = image.size;
  if (imageSize > size * 1024 * 1024) {
    throw `File is bigger than ${size}mb.`;
  }
  return true;
}
