import { Icon, Menu } from "antd";
import { WhatsAppIcon } from "../components";
import {
  FacebookShareButton,
  EmailShareButton,
  TwitterShareButton,
  WhatsappShareButton
} from "react-share";

const shareLinkMenu = link => {
  return (
    <Menu>
      <Menu.Item>
        <FacebookShareButton url={link}>
          <span>
            <Icon type="facebook" /> Facebook
          </span>
        </FacebookShareButton>
      </Menu.Item>
      <Menu.Item>
        <TwitterShareButton url={link}>
          <span>
            <Icon type="twitter" /> Tweet
          </span>
        </TwitterShareButton>
      </Menu.Item>
      <Menu.Item>
        <EmailShareButton url={link}>
          <span>
            <Icon type="mail" /> Mail
          </span>
        </EmailShareButton>
      </Menu.Item>
      <Menu.Item>
        <WhatsappShareButton url={link}>
          <span>
            <WhatsAppIcon /> WhatsApp
          </span>
        </WhatsappShareButton>
      </Menu.Item>
    </Menu>
  );
};
export default shareLinkMenu;
